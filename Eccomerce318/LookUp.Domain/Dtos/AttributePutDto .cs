﻿namespace LookUp.Domain.Dtos
{

    public class AttributePutDto
    {
        public Guid Id { get; set; }
        public AttributeTypeEnum Type { get; set; } 
        public string Unit { get; set; }
    }
}
