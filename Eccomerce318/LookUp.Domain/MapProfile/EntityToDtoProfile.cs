﻿using AutoMapper;
using LookUp.Domain.Dtos;
using LookUp.Domain.Entities;

namespace LookUp.Domain.MapProfile
{
    public class EntityToDtoProfile : Profile
    {
        public EntityToDtoProfile(): base("Entity to Dto Profile")
        {
            //Attribute Mapping 
            CreateMap<AttributeEntity,AttributeDto>();
            CreateMap<AttributeDto,AttributeEntity>();
            CreateMap<AttributeEntity,AttributePutDto>();
            CreateMap<AttributePutDto,AttributeEntity>();

            //Currency Mapping
            CreateMap<CurrenciesEntity, CurrenciesDto>();
            CreateMap<CurrenciesDto, CurrenciesEntity>();
            CreateMap<CurrenciesEntity, CurrenciesPutDto>();
            CreateMap<CurrenciesPutDto, CurrenciesEntity>();
        }
    }
}
