﻿using System.Net.NetworkInformation;

namespace LookUp.Domain.EventEnvelopes.Attribute
{
    public record AttributeUpdated(Guid id, AttributeTypeEnum Type, string Unit, DateTime Modified)
    {
        public static AttributeUpdated Update(
            Guid id,
            AttributeTypeEnum Type, 
            string Unit, 
            DateTime Modified) => 
            new (id, Type, Unit, Modified);
    }
}
