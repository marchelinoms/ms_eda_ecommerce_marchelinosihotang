﻿using System.Net.NetworkInformation;

namespace LookUp.Domain.EventEnvelopes.Attribute
{
    public record AttributeStatusChanged(Guid id,LookUpStatusEnum Status, DateTime Modified)
    {
        public static AttributeStatusChanged Change(
            Guid id,
            LookUpStatusEnum Status,
            DateTime Modified) => 
            new (id,Status, Modified);
    }
}
