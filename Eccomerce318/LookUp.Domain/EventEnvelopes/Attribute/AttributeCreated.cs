﻿using System.Net.NetworkInformation;

namespace LookUp.Domain.EventEnvelopes.Attribute
{
    public record AttributeCreated(Guid id, AttributeTypeEnum Type, string Unit, LookUpStatusEnum Status, DateTime Modified)
    {
        public static AttributeCreated Create(
            Guid id,
            AttributeTypeEnum Type, 
            string Unit, LookUpStatusEnum Status, 
            DateTime Modified) => 
            new (id, Type, Unit, Status, Modified);
    }
}
