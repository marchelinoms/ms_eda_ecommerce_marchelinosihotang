﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LookUp.Domain.Entities.Configurations
{
    public class AttributeConfiguration : IEntityTypeConfiguration<AttributeEntity>  
    {
        public void Configure(EntityTypeBuilder<AttributeEntity> builder)
        {
            builder.ToTable("Attributes");
            builder.HasKey(k => k.Id);
            builder.Property(k => k.Id ).IsRequired();
            builder.Property(k => k.Type).IsRequired();
            builder.Property(k => k.Unit).HasMaxLength(30).IsRequired();
            builder.Property(k => k.Status).IsRequired();
        }
    }
    public class CurrenciesConfiguration : IEntityTypeConfiguration<CurrenciesEntity>
    {
        public void Configure(EntityTypeBuilder<CurrenciesEntity> builder)
        {
            builder.ToTable("Currencies");
            builder.HasKey(k => k.Id);
            builder.Property(k => k.Name).HasMaxLength(30).IsRequired();
            builder.Property(k => k.Symbol).HasMaxLength(3).IsRequired();
            builder.Property(k => k.Code).HasMaxLength(3).IsRequired();
            builder.Property(k => k.Status).IsRequired();
        }
    }
}
