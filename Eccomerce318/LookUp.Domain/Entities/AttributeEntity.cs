﻿namespace LookUp.Domain.Entities
{
    public class AttributeEntity
    {
        public Guid Id { get; set; }
        public string Unit { get; set; } = default!;
        public AttributeTypeEnum Type { get; set; } = default!;
        public LookUpStatusEnum Status { get; set; } = default!;
        public DateTime Modified { get; internal set; } = DateTime.Now;
    }
}
