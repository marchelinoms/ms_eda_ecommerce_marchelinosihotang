﻿using LookUp.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace LookUp.Domain.Repositories
{
    public interface IAttributeRepository
    {
        Task<IEnumerable<AttributeEntity>> GetAll();
        Task<int> GetCount();
        Task<IEnumerable<AttributeEntity>> GetPaged(int Page, int Size);
        Task<AttributeEntity> GetById(Guid id);
        Task<AttributeEntity> Add(AttributeEntity entity);
        Task<AttributeEntity> Update(AttributeEntity entity);
        void Delete(AttributeEntity entity);
        Task<int> SaveChangesAsync(CancellationToken cancellationToken  = default);
    }
    public class AttributeRepository : IAttributeRepository
    {
        protected readonly LookUpDbContext _context;

        public AttributeRepository(LookUpDbContext context)
        {
            _context = context;
            _context.Database.EnsureCreated();
        }

        public async Task<AttributeEntity> Add(AttributeEntity entity)
        {
            _context.Set<AttributeEntity>().Add(entity);
            return entity;
        }

        public void Delete(AttributeEntity entity)
        {
            ;
            _context.Remove(entity);
            _context.SaveChanges();
            Console.WriteLine($"{entity} has been Deleted");
        }

        public async Task<IEnumerable<AttributeEntity>> GetAll()
        {
            return await _context.Set<AttributeEntity>().Where(x => !x.Status.Equals(LookUpStatusEnum.Removed)).ToListAsync();
        }

        public async Task<AttributeEntity> GetById(Guid id)
        {
            return await _context.Set<AttributeEntity>().FindAsync(id);
        }

        public Task<int> GetCount()
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<AttributeEntity>> GetPaged(int Page, int Size)
        {
            throw new NotImplementedException();
        }

        public async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            return await _context.SaveChangesAsync(cancellationToken);
        }

        public async Task<AttributeEntity> Update(AttributeEntity entity)
        {
            _context.Set<AttributeEntity>().Update(entity);
            return entity;
        }
        public virtual void Dispose(bool disposing) 
        {
            if (disposing)
                _context.Dispose();
        }
        public void Dispose() 
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
