﻿using FluentValidation;
using LookUp.Domain.Dtos;

namespace LookUp.Domain.Services.Validations
{
    public class CurrencyPostValidator : AbstractValidator<CurrenciesDto>
    {
        public CurrencyPostValidator()
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage("Name cannot be empty");
            RuleFor(x => x.Code).Length(3, 3).Matches(@"[A-Z]").WithMessage("Input the proper format for currency code");
            RuleFor(x => x.Symbol).Length(1, 3).Matches(@"^[0-9]").WithMessage("Input the proper format for currency symbol");
        }
    }
    public class CurrencyPutValidator : AbstractValidator<CurrenciesPutDto>
    {
        public CurrencyPutValidator()
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage("Name cannot be empty");
            RuleFor(x => x.Code).Length(3, 3).Matches(@"[A-Z]").WithMessage("Input the proper format for currency code");
            RuleFor(x => x.Symbol).Length(1, 3).Matches(@"^[0-9]").WithMessage("Input the proper format for currency symbol");
        }
    }
}
