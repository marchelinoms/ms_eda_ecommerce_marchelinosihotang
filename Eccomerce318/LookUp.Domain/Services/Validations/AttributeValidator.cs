﻿using FluentValidation;
using LookUp.Domain.Dtos;

namespace LookUp.Domain.Services.Validations
{
    public class AttributePostValidator : AbstractValidator<AttributeDto>
    {
        public AttributePostValidator()
        {
            RuleFor(x => x.Type).NotNull().WithMessage("Type cannot be null or empty").IsInEnum().WithMessage("Type must be registered in Enum");
            RuleFor(x => x.Unit).Length(5, 20).WithMessage("Unit must consist atleast 5 characters");
        }
    }
    public class AttributePutValidator : AbstractValidator<AttributePutDto>
    {
        public AttributePutValidator()
        {
            RuleFor(x => x.Type).NotNull().WithMessage("Type cannot be null or empty").IsInEnum().WithMessage("Type must be registered in Enum");
            RuleFor(x => x.Unit).Length(5, 20).WithMessage("Unit must consist atleast 5 characters");
        }
    }
}
