﻿using AutoMapper;
using Framework.Core.Events;
using Framework.Core.Events.Externals;
using LookUp.Domain.Dtos;
using LookUp.Domain.Entities;
using LookUp.Domain.EventEnvelopes.Attribute;
using LookUp.Domain.Repositories;

namespace LookUp.Domain.Services
{
    public interface IAttributeService
    {
        Task<IEnumerable<AttributeDto>> All();
        Task<AttributeDto> AddAttribute(AttributeDto dto);
        Task<AttributeDto> GetAttributeById(Guid id);
        Task<bool> UpdateAttribute(AttributePutDto dto);
        Task<bool> ActivateAttribute(Guid id);
        Task<bool> DeleteAttribute(Guid id);
    }
    public class AttributeService : IAttributeService
    {
        private IAttributeRepository _repository;
        private readonly IMapper _mapper;
        private readonly IExternalEventProducer _externalEventProducer;
        public AttributeService(IAttributeRepository repository, IMapper mapper, IExternalEventProducer externalEventProducer)
        {
            _repository = repository;
            _mapper = mapper;
            _externalEventProducer = externalEventProducer;
        }

        public async Task<AttributeDto> AddAttribute(AttributeDto dto)
        {
            if(dto != null)
            {
                dto.Status = LookUpStatusEnum.Inactive;
                var dtoToEntity = _mapper.Map<AttributeEntity>(dto);
                var entity = await _repository.Add(dtoToEntity);
                var result = await _repository.SaveChangesAsync();

                var externalEvent = new EventEnvelope<AttributeCreated>(
                    AttributeCreated.Create(
                        entity.Id,
                        entity.Type,
                        entity.Unit,
                        entity.Status,
                        entity.Modified
                        )
                    );
                if (result > 0)
                {
                    await _externalEventProducer.Publish(externalEvent, new CancellationToken());
                    return _mapper.Map<AttributeDto>(entity);
                }
            }

            return new AttributeDto();
        }

        public async Task<IEnumerable<AttributeDto>> All()
        {
            return _mapper.Map<IEnumerable<AttributeDto>>(await _repository.GetAll());
        }

        public async Task<AttributeDto> GetAttributeById(Guid id)
        {
            if(id != Guid.Empty)
            {
                var result = await _repository.GetById(id);
                if (result != null)
                {
                    return _mapper.Map<AttributeDto>(result);
                }
            }

            return null;
        }

        public async Task<bool> UpdateAttribute(AttributePutDto dto)
        {
            if(dto != null)
            {
                var attribute = await _repository.GetById(dto.Id);
                if (attribute != null)
                {
                    var entity = await _repository.Update(_mapper.Map<AttributeEntity>(dto));
                    entity.Status = attribute.Status;
                    var result = await _repository.SaveChangesAsync();

                    if (result > 0) 
                    {
                        var externalEvent = new EventEnvelope<AttributeUpdated>(
                            AttributeUpdated.Update(
                                entity.Id,
                                entity.Type,
                                entity.Unit,
                                entity.Modified
                             ));
                        await _externalEventProducer.Publish(externalEvent, new CancellationToken());
                        return true; 
                    }
                }
            }
            return false;
        }
        public async Task<bool> ActivateAttribute(Guid id)
        {
                var attribute = await _repository.GetById(id);
                if (attribute != null)
                {
                if (attribute.Status.Equals(LookUpStatusEnum.Inactive))
                {
                    attribute.Status = LookUpStatusEnum.Active;
                }
                else
                    attribute.Status = LookUpStatusEnum.Inactive;

                    var entity = await _repository.Update(_mapper.Map<AttributeEntity>(attribute));
                    var result = await _repository.SaveChangesAsync();
                if (result > 0) 
                {
                    var externalEvent = new EventEnvelope<AttributeStatusChanged>(
                        AttributeStatusChanged.Change(
                            entity.Id,
                            entity.Status,
                            entity.Modified
                            ));
                    await _externalEventProducer.Publish(externalEvent,new CancellationToken());
                    return true; 
                }
                }
            return false;
        }
        public async Task<bool> DeleteAttribute(Guid id)
        {
                var attribute = await _repository.GetById(id);
            if (attribute != null)
            {
                attribute.Status = LookUpStatusEnum.Removed;
                var entity = await _repository.Update(_mapper.Map<AttributeEntity>(attribute));
                var result = await _repository.SaveChangesAsync();
                if (result > 0)
                {
                    var externalEvent = new EventEnvelope<AttributeStatusChanged>(
                        AttributeStatusChanged.Change(
                            entity.Id,
                            entity.Status,
                            entity.Modified
                            ));
                    await _externalEventProducer.Publish(externalEvent, new CancellationToken());
                    return true;
                }
            }
            return false;
        }
    }
}
