﻿using AutoMapper;
using LookUp.Domain.Dtos;
using LookUp.Domain.Entities;
using LookUp.Domain.Repositories;


namespace LookUp.Domain.Services
{
    public interface ICurrencyService
    {
        Task<IEnumerable<CurrenciesDto>> All();
        Task<CurrenciesDto> AddCurrency(CurrenciesDto dto);
        Task<CurrenciesDto> GetCurrencyById(Guid id);
        Task<bool> UpdateCurrency(CurrenciesPutDto dto);
        Task<bool> ActivateCurrency(Guid id);
        Task<bool> DeleteCurrency(Guid id);
    }
    public class CurrencyService : ICurrencyService
    {
        private ICurrencyRepository _repository;
        private readonly IMapper _mapper;

        public CurrencyService(ICurrencyRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }
        public async Task<bool> ActivateCurrency(Guid id)
        {

            var currencies = await _repository.GetById(id);
            if (currencies != null)
            {
                if (currencies.Status.Equals(LookUpStatusEnum.Inactive))
                {
                    currencies.Status = LookUpStatusEnum.Active;
                }
                else 
                    currencies.Status = LookUpStatusEnum.Inactive;

                var entity = await _repository.Update(_mapper.Map<CurrenciesEntity>(currencies));
                var result = await _repository.SaveChangesAsync();
                if (result > 0) return true;
            }
            return false;
        }

        public async Task<CurrenciesDto> AddCurrency(CurrenciesDto dto)
        {
            if (dto != null)
            {
                dto.Status = LookUpStatusEnum.Inactive;
                var dtoToEntity = _mapper.Map<CurrenciesEntity>(dto);
                var entity = await _repository.Add(dtoToEntity);
                var result = await _repository.SaveChangesAsync();

                if (result > 0)
                    return _mapper.Map<CurrenciesDto>(entity);
            }

            return new CurrenciesDto();
        }

        public async Task<IEnumerable<CurrenciesDto>> All()
        {
            return _mapper.Map<IEnumerable<CurrenciesDto>>(await _repository.GetAll());
        }
        public async Task<bool> DeleteCurrency(Guid id)
        {
            var currencies = await _repository.GetById(id);
            if (currencies != null)
            {
                currencies.Status = LookUpStatusEnum.Removed;
                var entity = await _repository.Update(_mapper.Map<CurrenciesEntity>(currencies));
                var result = await _repository.SaveChangesAsync();
                if (result > 0) return true;
            }
            return false;
        }

        public async Task<CurrenciesDto> GetCurrencyById(Guid id)
        {
            if (id != Guid.Empty)
            {
                var result = await _repository.GetById(id);
                if (result != null)
                {
                    return _mapper.Map<CurrenciesDto>(result);
                }
            }
            return null;
        }

        public async Task<bool> UpdateCurrency(CurrenciesPutDto dto)
        {
            if (dto != null)
            {
                
                var currencies = await _repository.GetById(dto.Id);
                if (currencies != null)
                {
                    var entity = await _repository.Update(_mapper.Map<CurrenciesEntity>(dto));
                    entity.Status = currencies.Status;
                    var result = await _repository.SaveChangesAsync();
                    if (result > 0) return true;
                }
            }
            return false;
        }
    }
}
