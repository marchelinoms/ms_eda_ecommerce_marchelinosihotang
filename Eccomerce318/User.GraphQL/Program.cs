using Microsoft.EntityFrameworkCore;
using User.Domain;
using Framework.Kafka;
using Framework.Core.Events;
using User.Domain.MapProfile;
using User.Domain.Repositories;
using User.Domain.Services;
using User.GraphQL.Schema.Queries;
using User.GraphQL.Schema.Mutations;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;

internal class Program
{
    private static void Main(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);
        // Add services to the container.
        builder.Services.AddDomainContext(options =>
        {
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile(
                "appsettings.json", optional: true, reloadOnChange: true);
            options.UseSqlServer(builder.Build().GetSection("ConnectionStrings").GetSection("User_Db_Conn").Value);
            options.EnableSensitiveDataLogging(false).UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
        });
        builder.Services.AddControllers();
        builder.Services.AddAutoMapper(config =>
        {
            config.AddProfile<EntityToDtoProfile>();
        });

        // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
        builder.Services.AddEndpointsApiExplorer();
        builder.Services.AddSwaggerGen();

        builder.Services.AddAuthentication(options =>
        {
            options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
        })
            .AddJwtBearer("Bearer", options =>
            {
                var Configuration = builder.Configuration;
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = Configuration["JWT:ValidIssuer"],
                    ValidAudience = Configuration["JWT:ValidAudience"],
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["JWT:Secret"]))
                };
                options.Events = new JwtBearerEvents
                {
                    OnChallenge = context =>
                    {
                        context.Response.OnStarting(async () =>
                        {
                            await context.Response.WriteAsync("Account not authorized");
                        });
                        return Task.CompletedTask;
                    },
                    OnForbidden = context =>
                    {
                        context.Response.OnStarting(async () =>
                        {
                            await context.Response.WriteAsync("Forbidden Account");
                        });
                        return Task.CompletedTask;
                    }
                };
            });
        builder.Services.AddEventBus();
        builder.Services.AddKafkaProducer();
        builder.Services
            .AddScoped<UserQuery>()
            .AddScoped<UserMutation>()
            .AddScoped<AuthQuery>()
            .AddScoped<IUserRepository, UserRepository>()
            .AddScoped<IUserService, UserService>()
            .AddGraphQLServer()
            .AddAuthorization()
            .AddType<UserQuery>()
            .AddType<UserMutation>()
            .AddType<AuthQuery>()
            .AddQueryType(q => q.Name("UserQuery"))
            .AddMutationType(q => q.Name("UserMutation"));


        var app = builder.Build();

        // Configure the HTTP request pipeline.
        if (app.Environment.IsDevelopment())
        {
            app.UseSwagger();
            app.UseSwaggerUI();
        }

        app.UseHttpsRedirection();

        app.UseAuthorization();

        app.UseAuthentication();

        app.MapControllers();

        app.MapGraphQL();

        app.Run();
    }
}