﻿using Framework.Auth;
using HotChocolate.Authorization;
using User.Domain.Dtos;
using User.Domain.Services;

namespace User.GraphQL.Schema.Mutations
{
    [ExtendObjectType(Name ="UserMutation")]
    public class UserMutation
    {
        private readonly IUserService _service;
        public UserMutation(IUserService service)
        {
            _service = service;
        }

        public async Task<UserDto> AddUser(UserInputDto user)
        {
            UserDto dto = new UserDto();
            dto.UserName = user.UserName;
            dto.Email = user.Email;
            dto.Password = Encryption.HashSha256(user.Password);
            dto.FirstName = user.FirstName;
            dto.LastName = user.LastName;
            dto.Type = user.Type;
            return await _service.AddUser(dto);
        }
        [Authorize(Roles = new[] {"admin"})]
        public async Task<UserPutDto> UpdateUser(UserInputDto user, Guid id)
        {
            UserPutDto dto = new UserPutDto();
            dto.Id = id;
            dto.UserName = user.UserName;
            dto.Email = user.Email;
            dto.FirstName = user.FirstName;
            dto.LastName = user.LastName;
            dto.Type = user.Type;
            var result = await _service.UpdateUser(dto);
            return result ? dto : throw new GraphQLException(new Error("User not Found", "404"));
        }
        [Authorize]
        public async Task<UserDto> UpdateUserStatus(Guid id)
        {
            UserDto dto = await _service.GetUserById(id);
            var result = await _service.ActivateUser(id);
            return result ? dto : throw new GraphQLException(new Error("User not Found", "404"));
        }
        [Authorize]
        public async Task<string> DeleteUser(Guid id)
        {
            UserDto dto = await _service.GetUserById(id);
            var result = await _service.DeleteUser(id);
            return result ? dto.Id + " has been deleted": throw new GraphQLException(new Error("User not Found", "404"));
        }
    }
}
