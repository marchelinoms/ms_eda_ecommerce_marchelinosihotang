﻿using HotChocolate.Authorization;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using User.Domain.Dtos;
using User.Domain.Services;

namespace User.GraphQL.Schema.Queries
{
    [ExtendObjectType(Name = "UserQuery")]
    public class AuthQuery
    {
        private readonly IUserService service;
        private readonly IConfiguration configuration;

        public AuthQuery(IUserService _service, IConfiguration _configuration)
        {
            service = _service;
            configuration = _configuration;
        }
        public string IndexAll()
        {
            return "This for all";
        }
        [Authorize]
        public string IndexAuthorized()
        {
            return "This for all authorized";
        }
        [Authorize(Roles = new[] {"customer"})]
        public string IndexCustomer()
        {
            return "This is for customer";
        }
        [Authorize(Roles = new[] {"admin"})]
        public string IndexAdmin()
        {
            return "This is for Admin";
        }
        public async Task<LoginDto> Login(string username,string password)
        {
            LoginDto dto = await service.Login(username, password);
            if (dto != null)
            {
                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name,dto.UserName),
                    new Claim("FullName", dto.FullName)
                };
                foreach(var item in dto.Roles)
                {
                    claims.Add(new Claim(ClaimTypes.Role, item.ToLower()));
                }
                var token = GetToken(claims);
                dto.Token = new JwtSecurityTokenHandler().WriteToken(token);
                dto.Expiration = token.ValidTo;
            }
            return dto;
        }
        private JwtSecurityToken GetToken(List<Claim> authClaims)
        {
            var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["JWT:Secret"]));
            var jwt = new JwtSecurityToken(
                issuer: configuration["JWT:ValidIssuer"],
                audience: configuration["JWT:ValidAudience"],
                expires: DateTime.Now.AddDays(Convert.ToDouble(configuration["JWT:ExpireDays"])),
                claims: authClaims,
                signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256)
                );
            return jwt;
        }
    }
}
