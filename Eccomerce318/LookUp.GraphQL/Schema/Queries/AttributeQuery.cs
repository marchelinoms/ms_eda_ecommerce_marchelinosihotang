﻿using LookUp.Domain.Dtos;
using LookUp.Domain.Services;

namespace LookUp.GraphQL.Schema.Queries
{
    [ExtendObjectType (Name = "LookUpQuery")]
    public class AttributeQuery
    {
        private readonly IAttributeService _service;

        public AttributeQuery(IAttributeService service)
        {
            _service = service;
        }
        public async Task<IEnumerable<AttributeDto>> GetAttributeAll()
        {
            IEnumerable<AttributeDto> result = await _service.All();
            return result;
        }
        public async Task<AttributeDto> GetAttributeById(Guid id)
        {
            return await _service.GetAttributeById(id);
        }
    }
}
