﻿using LookUp.Domain.Dtos;
using LookUp.Domain.Services;

namespace LookUp.GraphQL.Schema.Queries
{
    [ExtendObjectType(Name = "LookUpQuery")]
    public class CurrencyQuery
    {
        private readonly ICurrencyService _service;
        public CurrencyQuery(ICurrencyService service)
        {
            _service = service;
        }
        public async Task<IEnumerable<CurrenciesDto>> GetCurrencyAll()
        {
            IEnumerable<CurrenciesDto> result = await _service.All();
            return result;
        }
        public async Task<CurrenciesDto> GetCurrencyById(Guid id)
        {
            return await _service.GetCurrencyById(id);
        }
    }
}
