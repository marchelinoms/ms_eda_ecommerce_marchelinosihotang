﻿using LookUp.Domain.Dtos;
using LookUp.Domain.Services;
using LookUp.GraphQL.Schema.Mutations.Inputs;
using HotChocolate.Authorization;

namespace LookUp.GraphQL.Schema.Mutations
{
    [ExtendObjectType(Name = "LookUpMutation")]
    public class AttributeMutation
    {
        private readonly IAttributeService _service;
        public AttributeMutation(IAttributeService service)
        {
            _service = service;
        }
        [Authorize(Roles = new[] { "admin" })]
        public async Task<AttributeDto> AddAttribute(AttributeTypeInput attribute)
        {
            AttributeDto dto = new AttributeDto();
            dto.Unit = attribute.Unit;
            dto.Type = attribute.Type;
            return await _service.AddAttribute(dto);
        }
        [Authorize(Roles = new[] { "admin" })]
        public async Task<AttributePutDto> UpdateAttribute(AttributeTypeInput attribute, Guid id)
        {
            AttributePutDto dto = new AttributePutDto();
            dto.Id = id;
            dto.Unit = attribute.Unit;
            dto.Type = attribute.Type;
            var result = await _service.UpdateAttribute(dto);

            return result ? dto : throw new GraphQLException(new Error ("Attribute not Found","404"));
        }
        [Authorize(Roles = new[] { "admin" })]
        public async Task<AttributeDto> UpdateAttributeStatus(Guid id)
        {
            AttributeDto dto = await _service.GetAttributeById(id);
            var result = await _service.ActivateAttribute(id);
            return result ? dto : throw new GraphQLException(new Error("Attribute not Found", "404"));
        }
        [Authorize(Roles = new[] { "admin" })]
        public async Task<string> DeleteAttribute(Guid id)
        {
            AttributeDto dto = await _service.GetAttributeById(id);
            var result = await _service.DeleteAttribute(id);
            return result ? dto.Id + " has been deleted" : throw new GraphQLException(new Error("Attribute not Found", "404"));
        }
    }
}
