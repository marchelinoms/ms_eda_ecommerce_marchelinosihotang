﻿namespace LookUp.GraphQL.Schema.Mutations.Inputs
{
    public class CurrencyTypeInput
    {
        public string Name { get; set; } = default!;
        public string Code { get; set; } = default!;
        public string Symbol { get; set; } = default!;
    }
}
