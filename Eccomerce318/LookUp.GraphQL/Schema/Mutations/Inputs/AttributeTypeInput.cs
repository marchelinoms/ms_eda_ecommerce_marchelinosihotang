﻿using LookUp.Domain;

namespace LookUp.GraphQL.Schema.Mutations.Inputs
{
    public class AttributeTypeInput
    {
        public AttributeTypeEnum Type { get; set; }
        public string Unit { get; set; }
    }
}
