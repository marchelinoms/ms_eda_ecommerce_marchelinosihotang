﻿using LookUp.Domain.Dtos;
using LookUp.Domain.Services;
using LookUp.GraphQL.Schema.Mutations.Inputs;
using HotChocolate.Authorization;

namespace LookUp.GraphQL.Schema.Mutations
{
    [ExtendObjectType(Name = "LookUpMutation")]
    public class CurrencyMutation
    {
        private readonly ICurrencyService _service;
        public CurrencyMutation(ICurrencyService service)
        {
            _service = service;
        }
        [Authorize(Roles = new[] { "admin" })]
        public async Task<CurrenciesDto> AddCurrency(CurrencyTypeInput currency)
        {
            CurrenciesDto dto = new CurrenciesDto();
            dto.Code = currency.Code;
            dto.Name = currency.Name;
            dto.Symbol = currency.Symbol;
            return await _service.AddCurrency(dto);
        }
        [Authorize(Roles = new[] { "admin" })]
        public async Task<CurrenciesPutDto> UpdateCurrency(CurrencyTypeInput currency, Guid id)
        {
            CurrenciesPutDto dto = new CurrenciesPutDto();
            dto.Id = id;
            dto.Code = currency.Code;
            dto.Name = currency.Name;
            dto.Symbol = currency.Symbol;
            var result = await _service.UpdateCurrency(dto);

            return result ? dto : throw new GraphQLException(new Error ("Currency not Found","404"));
        }
        [Authorize(Roles = new[] { "admin" })]
        public async Task<CurrenciesDto> UpdateCurrencyStatus(Guid id)
        {
            CurrenciesDto dto = await _service.GetCurrencyById(id);
            var result = await _service.ActivateCurrency(id);
            return result ? dto : throw new GraphQLException(new Error("Currency not Found", "404"));
        }

        [Authorize(Roles = new[] { "admin" })]
        public async Task<string> DeleteCurrency(Guid id)
        {
            CurrenciesDto dto = await _service.GetCurrencyById(id);
            var result = await _service.DeleteCurrency(id);
            return result ? dto.Id + " has been deleted" : throw new GraphQLException(new Error("Currency not Found", "404"));
        }
    }
}
