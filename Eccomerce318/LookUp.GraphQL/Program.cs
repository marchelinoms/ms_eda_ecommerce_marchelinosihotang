using Framework.Kafka;
using LookUp.Domain;
using LookUp.Domain.MapProfile;
using LookUp.Domain.Repositories;
using LookUp.Domain.Services;
using LookUp.GraphQL.Schema.Mutations;
using LookUp.GraphQL.Schema.Queries;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace LookUp.GraphQL
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.
            builder.Services.AddDomainContext(options =>
            {
                var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile(
                    "appsettings.json", optional: true, reloadOnChange: true);
                options.UseSqlServer(builder.Build().GetSection("ConnectionStrings").GetSection("LookUp_Db_Conn").Value);
                options.EnableSensitiveDataLogging(false).UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            });

            builder.Services.AddControllers();


            builder.Services.AddAutoMapper(config =>
            {
                config.AddProfile<EntityToDtoProfile>();
            });

            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();
            builder.Services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
       .AddJwtBearer("Bearer", options =>
       {
           var Configuration = builder.Configuration;
           options.TokenValidationParameters = new TokenValidationParameters
           {
               ValidateIssuer = true,
               ValidateAudience = true,
               ValidateLifetime = true,
               ValidateIssuerSigningKey = true,
               ValidIssuer = Configuration["JWT:ValidIssuer"],
               ValidAudience = Configuration["JWT:ValidAudience"],
               IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["JWT:Secret"]))
           };
           options.Events = new JwtBearerEvents
           {
               OnChallenge = context =>
               {
                   context.Response.OnStarting(async () =>
                   {
                       await context.Response.WriteAsync("Account not authorized");
                   });
                   return Task.CompletedTask;
               },
               OnForbidden = context =>
               {
                   context.Response.OnStarting(async () =>
                   {
                       await context.Response.WriteAsync("Forbidden Account");
                   });
                   return Task.CompletedTask;
               }
           };
       });
            builder.Services.AddKafkaProducer();
            builder.Services
                .AddScoped<AttributeQuery>()
                .AddScoped<CurrencyQuery>()
                .AddScoped<AttributeMutation>()
                .AddScoped<CurrencyMutation>()
                .AddScoped<IAttributeRepository, AttributeRepository>()
                .AddScoped<IAttributeService, AttributeService>()
                .AddScoped<ICurrencyRepository, CurrencyRepository>()
                .AddScoped<ICurrencyService, CurrencyService>()
                .AddGraphQLServer()
                .AddAuthorization()
                .AddType<AttributeQuery>()
                .AddType<CurrencyQuery>()
                .AddType<AttributeMutation>()
                .AddType<CurrencyMutation>()
                .AddQueryType(q => q.Name("LookUpQuery"))
                .AddMutationType(m => m.Name("LookUpMutation"));
            

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseHttpsRedirection();

            app.UseAuthorization();

            app.UseAuthentication();

            app.MapControllers();

            app.MapGraphQL();

            app.Run();
        }
    }
}