﻿using FluentValidation.Results;
using FluentValidation;
using LookUp.Domain.Dtos;
using LookUp.Domain.Services;
using Microsoft.AspNetCore.Mvc;

namespace LookUp.OpenAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AttributeController : ControllerBase
    {
        private readonly IAttributeService _service;
        private readonly ILogger <AttributeController> _logger;
        private readonly IValidator<AttributeDto> _validatorPost;
        private readonly IValidator<AttributePutDto> _validatorPut;

        public AttributeController(IAttributeService service, ILogger<AttributeController> logger, IValidator<AttributeDto> validatorPost, IValidator<AttributePutDto> validatorPut)
        {
            _service = service;
            _logger = logger;
            _validatorPost = validatorPost;
            _validatorPut = validatorPut;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(await _service.All());
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] AttributeDto payload, CancellationToken cancellation)
        {
            try
            {
                ValidationResult result = await _validatorPost.ValidateAsync(payload);
                if (!result.IsValid) return BadRequest(result);
                var dto = await _service.AddAttribute(payload);
                if (dto != null) return Ok(dto);
            }
            catch (OperationCanceledException ex) when (cancellation.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
            }
            return BadRequest();
        }

        [HttpPut]
        public async Task<IActionResult> Edit([FromBody] AttributePutDto payload, CancellationToken cancellation)
        {
            try
            {

                ValidationResult result = await _validatorPut.ValidateAsync(payload);
                if (!result.IsValid) return BadRequest(result);
                if (payload != null)
                {
                    var isUpdated = await _service.UpdateAttribute(payload);
                    return isUpdated ? Ok(isUpdated) : BadRequest();
                }
            }
            catch (OperationCanceledException ex) when (cancellation.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
            }
            return NoContent();
        }

        [HttpPut("Status")]
        public async Task<IActionResult> Activate(Guid id, CancellationToken cancellation)
        {
            try
            {
                    var isUpdated = await _service.ActivateAttribute(id);
                    return isUpdated ? Ok(isUpdated) : BadRequest();
                
            }
            catch (OperationCanceledException ex) when (cancellation.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
            }
            return NoContent();
        }
        
        [HttpDelete]
        public async Task<IActionResult> Delete(Guid id, CancellationToken cancellation)
        {
            try
            {
                    var isUpdated = await _service.DeleteAttribute(id);
                    return isUpdated ? Ok(isUpdated) : BadRequest();
                
            }
            catch (OperationCanceledException ex) when (cancellation.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
            }
            return NoContent();
        }

    }
}
