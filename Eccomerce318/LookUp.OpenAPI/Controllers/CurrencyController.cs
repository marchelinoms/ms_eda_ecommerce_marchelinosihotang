﻿using FluentValidation;
using FluentValidation.Results;
using LookUp.Domain.Dtos;
using LookUp.Domain.Services;
using Microsoft.AspNetCore.Mvc;

namespace LookUp.OpenAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CurrencyController : ControllerBase
    {
        private readonly ICurrencyService _service;
        private readonly ILogger<CurrencyController> _logger;
        private readonly IValidator<CurrenciesDto> _validatorPost;
        private readonly IValidator<CurrenciesPutDto> _validatorPut;


        public CurrencyController(ICurrencyService service, ILogger<CurrencyController> logger, IValidator<CurrenciesDto> validatorPost, IValidator<CurrenciesPutDto> validatorPut)
        {
            _service = service;
            _logger = logger;
            _validatorPost = validatorPost;
            _validatorPut = validatorPut;
        }
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(await _service.All());
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CurrenciesDto payload, CancellationToken cancellation)
        {
            try
            {
                ValidationResult result = await _validatorPost.ValidateAsync(payload);
                if (!result.IsValid) return BadRequest(payload);
                var dto = await _service.AddCurrency(payload);
                if (dto != null)
                    return Ok(dto);
            }
            catch (OperationCanceledException ex) when (cancellation.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
            }
            return BadRequest();
        }

        [HttpPut]
        public async Task<IActionResult> Edit([FromBody] CurrenciesPutDto payload,Guid id, CancellationToken cancellation)
        {
            try
            {
                if (payload != null)
                {
                    payload.Id = id;
                    var isUpdated = await _service.UpdateCurrency(payload);
                    return isUpdated ? Ok(isUpdated) : BadRequest();
                }
            }
            catch (OperationCanceledException ex) when (cancellation.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
            }
            return NoContent();
        }

        [HttpPut("Status")]
        public async Task<IActionResult> Activate(Guid id, CancellationToken cancellation)
        {
            try
            {
                var isUpdated = await _service.ActivateCurrency(id);
                return isUpdated ? Ok(isUpdated) : BadRequest();

            }
            catch (OperationCanceledException ex) when (cancellation.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
            }
            return NoContent();
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(Guid id, CancellationToken cancellation)
        {
            try
            {
                var isUpdated = await _service.DeleteCurrency(id);
                return isUpdated ? Ok(isUpdated) : BadRequest();

            }
            catch (OperationCanceledException ex) when (cancellation.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
            }
            return NoContent();
        }
    }
}
