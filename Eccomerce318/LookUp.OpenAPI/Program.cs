using FluentValidation;
using Framework.Kafka;
using LookUp.Domain;
using LookUp.Domain.MapProfile;
using LookUp.Domain.Repositories;
using LookUp.Domain.Services;
using LookUp.Domain.Services.Validations;
using Microsoft.EntityFrameworkCore;


var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddDomainContext(options =>
{
    var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile(
        "appsettings.json", optional: true, reloadOnChange: true);
    options.UseSqlServer(builder.Build().GetSection("ConnectionStrings").GetSection("LookUp_Db_Conn").Value);
    options.EnableSensitiveDataLogging(false).UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
});

builder.Services
    .AddScoped<IAttributeService, AttributeService>()
    .AddScoped<IAttributeRepository, AttributeRepository>();
builder.Services
    .AddScoped<ICurrencyService, CurrencyService>()
    .AddScoped<ICurrencyRepository, CurrencyRepository>();

builder.Services.AddValidatorsFromAssemblyContaining<AttributePostValidator>();
builder.Services.AddValidatorsFromAssemblyContaining<AttributePutValidator>();
builder.Services.AddValidatorsFromAssemblyContaining<CurrencyPostValidator>();
builder.Services.AddValidatorsFromAssemblyContaining<CurrencyPutValidator>();

builder.Services.AddControllers();

builder.Services.AddAutoMapper(config =>
{
    config.AddProfile<EntityToDtoProfile>();
});

builder.Services.AddKafkaProducer();


// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
