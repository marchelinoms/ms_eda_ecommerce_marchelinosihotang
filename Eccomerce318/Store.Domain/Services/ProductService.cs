﻿using AutoMapper;
using Framework.Core.Events;
using Framework.Core.Events.Externals;
using Store.Domain.Dtos;
using Store.Domain.Entities;
using Store.Domain.EventEnvelopes;
using Store.Domain.Repositories;

namespace Store.Domain.Services
{
    public interface IProductService
    {
        Task<IEnumerable<ProductDto>> All();
        Task<ProductDto> AddProduct(ProductDto dto);
        Task<ProductDto> GetProductById(Guid id);
        Task<bool> UpdateProduct(ProductPutDto dto);
        Task<bool> ActivateProduct(Guid id);
        Task<bool> DeleteProduct(Guid id);
    }
    public class ProductService : IProductService
    {
        private IProductRepository _repository;
        private readonly IMapper _mapper;
        private readonly IExternalEventProducer _externalEventProducer;
        public ProductService(IProductRepository repository, IMapper mapper, IExternalEventProducer externalEventProducer)
        {
            _repository = repository;
            _mapper = mapper;
            _externalEventProducer = externalEventProducer;
        }

        public async Task<ProductDto> AddProduct(ProductDto dto)
        {
            if(dto != null)
            {
                dto.Status = StoreStatusEnum.Inactive;
                var entity = await _repository.Add(_mapper.Map<ProductEntity>(dto));
                var result = await _repository.SaveChangesAsync();
                var externalEvent = new EventEnvelope<ProductCreated>(
                    ProductCreated.Create(
                        entity.Id,
                        entity.CategoryId,
                        entity.AttributeId,
                        entity.SKU,
                        entity.Name,
                        entity.Description,
                        (decimal)entity.Price,
                        (decimal)entity.Volume,
                        entity.Sold,
                        entity.Stock,
                        entity.Status,
                        entity.Modified
                        )
                    );
                if(result > 0)
                {
                    await _externalEventProducer.Publish    (externalEvent, new CancellationToken());
                    return _mapper.Map<ProductDto>(entity);
                }
            }
            return new ProductDto();
        }

        public async Task<IEnumerable<ProductDto>> All()
        {
            return _mapper.Map<IEnumerable<ProductDto>>(await _repository.GetAll());
        }

        public async Task<ProductDto> GetProductById(Guid id)
        {
            if(id != Guid.Empty)
            {
                var result = await _repository.GetById(id);
                if (result != null)
                {
                    return _mapper.Map<ProductDto>(result);
                }
            }

            return null;
        }

        public async Task<bool> UpdateProduct(ProductPutDto dto)
        {
            if(dto != null)
            {
                var product = await _repository.GetById(dto.Id);
                if (product != null)
                {
                    var entity = await _repository.Update(_mapper.Map<ProductEntity>(dto));
                    entity.Status = product.Status;
                    var result = await _repository.SaveChangesAsync();
                    if(result > 0)
                    {
                        var externalEvent = new EventEnvelope<ProductUpdated>(
                            ProductUpdated.Update(
                                entity.Id,
                                entity.CategoryId,
                                entity.AttributeId,
                                entity.SKU,
                                entity.Name,
                                entity.Description,
                                (decimal)entity.Price,
                                (decimal)entity.Volume,
                                entity.Sold,
                                entity.Stock,
                                entity.Modified
                                )
                            );
                        await _externalEventProducer.Publish(externalEvent, new CancellationToken());
                        return true;
                    }
                }
            }
            return false;
        }
        public async Task<bool> ActivateProduct(Guid id)
        {
                var product = await _repository.GetById(id);
                if (product != null)
                {
                    product.Status = StoreStatusEnum.Active;
                    var entity = await _repository.Update(_mapper.Map<ProductEntity>(product));
                    var result = await _repository.SaveChangesAsync();
                if (result > 0) 
                {
                    var externalEvent = new EventEnvelope<ProductStatusChanged>(
                        ProductStatusChanged.Chang(
                            entity.Id,
                            entity.Status,
                            entity.Modified
                            ));
                    await _externalEventProducer.Publish(externalEvent, new CancellationToken());
                    return true; 
                }
                }
            return false;
        }
        public async Task<bool> DeleteProduct(Guid id)
        {
                var product = await _repository.GetById(id);
                if (product != null)
                {
                    product.Status = StoreStatusEnum.Removed;
                    var entity = await _repository.Update(_mapper.Map<ProductEntity>(product));
                    var result = await _repository.SaveChangesAsync();
                if (result > 0)
                {
                    var externalEvent = new EventEnvelope<ProductStatusChanged>(
                        ProductStatusChanged.Chang(
                            entity.Id,
                            entity.Status,
                            entity.Modified
                            ));
                    await _externalEventProducer.Publish(externalEvent, new CancellationToken());
                    return true;
                }
            }
            return false;
        }
    }
}
