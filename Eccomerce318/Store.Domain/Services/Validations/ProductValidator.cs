﻿using FluentValidation;
using Store.Domain.Dtos;

namespace Store.Domain.Services.Validations
{
    
    public class ProductPostValidator : AbstractValidator<ProductDto>
    {
        public ProductPostValidator()
        {
            int result = 0;
            RuleFor(x => x.Name).NotNull().WithMessage("Name cannot be null/empty");
            RuleFor(x => x.Description).Length(5,255).WithMessage("Description must consist at least 5 characters");
            RuleFor(x => x.SKU).Length(5,20).WithMessage("SKU must consist at least 5 characters")
                .Must(x => int.TryParse(x,out result)).WithMessage("SKU must consist of numbers");
            RuleFor(x => x.Price).NotEmpty().WithMessage("Price cannot be empty");
            RuleFor(x => x.Volume).Must(x => x > 0).WithMessage("Volume cannot be zero");
            RuleFor(x => x.Stock).NotEmpty().WithMessage("Stock cannot be empty");
        }
    }
    public class ProductPutValidator : AbstractValidator<ProductPutDto>
    {
        public ProductPutValidator()
        {
            int result = 0;
            RuleFor(x => x.Name).NotNull().WithMessage("Name cannot be null/empty");
            RuleFor(x => x.Description).Length(5, 255).WithMessage("Description must consist at least 5 characters");
            RuleFor(x => x.SKU).Length(5, 20).WithMessage("SKU must consist at least 5 characters")
                .Must(x => int.TryParse(x,out result)).WithMessage("SKU must consist of numbers");
            RuleFor(x => x.Price).NotEmpty().WithMessage("Price cannot be empty");
            RuleFor(x => x.Volume).Must(x => x > 0).WithMessage("Volume cannot be zero");
            RuleFor(x => x.Stock).NotEmpty().WithMessage("Stock cannot be empty");
        }
    }
}
