﻿using FluentValidation;
using Store.Domain.Dtos;

namespace Store.Domain.Services.Validations
{
    public class CategoryPostValidator : AbstractValidator<CategoryDto>
    {
        public CategoryPostValidator() 
        {
            RuleFor(x => x.Name).NotNull().WithMessage("Name cannot be null/empty");
            RuleFor(x => x.Description).Length(5,255).WithMessage("Description must consist at least 5 characters");
        }
    }
    public class CategoryPutValidator : AbstractValidator<CategoryPutDto>
    {
        public CategoryPutValidator() 
        {
            RuleFor(x => x.Name).NotNull().WithMessage("Name cannot be null/empty");
            RuleFor(x => x.Description).Length(5,255).WithMessage("Description must consist at least 5 characters");
        }
    }
}
