﻿using AutoMapper;
using Store.Domain.Dtos;
using Store.Domain.Entities;
using Store.Domain.Repositories;

namespace Store.Domain.Services
{
    public interface IGalleryService
    {
        Task<GalleryDto> AddGallery(GalleryDto dto);
        Task<GalleryDto> GetGalleryById(Guid id);
        Task<IEnumerable<GalleryDto>> All();
    }
    public class GalleryService : IGalleryService
    {
        private readonly IGalleryRepostitory repository;
        private readonly IMapper mapper;
        public GalleryService(IGalleryRepostitory _repository, IMapper _mapper)
        {
            repository = _repository;
            mapper = _mapper;
        }

        public async Task<GalleryDto> AddGallery(GalleryDto dto)
        {
           if(dto != null)
            {
                dto.Status = StoreStatusEnum.Active;
                var entity = await repository.Add(mapper.Map<GalleryEntity>(dto));
                var result = await repository.SaveChangesAsync();
                if(result > 0)
                    return mapper.Map<GalleryDto>(entity);
            }
            return dto;
        }

        public async Task<IEnumerable<GalleryDto>> All()
        {
            return mapper.Map<IEnumerable<GalleryDto>>(await repository.GetAll());    
        }

        public async Task<GalleryDto> GetGalleryById(Guid id)
        {
            return mapper.Map<GalleryDto>(await repository.GetById(id));
        }
    }
}
