﻿using AutoMapper;
using Store.Domain.Dtos;
using Store.Domain.Entities;
using Store.Domain.Repositories;

namespace Store.Domain.Services
{
    public interface ICategoryService
    {
        Task<IEnumerable<CategoryDto>> All();
        Task<CategoryDto> AddCategory(CategoryDto dto);
        Task<CategoryDto> GetCategoryById(Guid id);
        Task<bool> UpdateCategory(CategoryPutDto dto);
        Task<bool> ActivateCategory(Guid id);
        Task<bool> DeleteCategory(Guid id);
    }
    public class CategoryService : ICategoryService
    {
        private ICategoryRepository _repository;
        private readonly IMapper _mapper;
        public CategoryService(ICategoryRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<CategoryDto> AddCategory(CategoryDto dto)
        {
            if(dto != null)
            {
                dto.Status = StoreStatusEnum.Inactive;
                var dtoToEntity = _mapper.Map<CategoryEntity>(dto);
                var entity = await _repository.Add(dtoToEntity);
                var result = await _repository.SaveChangesAsync();

                if(result > 0)
                    return _mapper.Map<CategoryDto>(entity);
            }

            return new CategoryDto();
        }

        public async Task<IEnumerable<CategoryDto>> All()
        {
            return _mapper.Map<IEnumerable<CategoryDto>>(await _repository.GetAll());
        }

        public async Task<CategoryDto> GetCategoryById(Guid id)
        {
            if(id != Guid.Empty)
            {
                var result = await _repository.GetById(id);
                if (result != null)
                {
                    return _mapper.Map<CategoryDto>(result);
                }
            }

            return null;
        }

        public async Task<bool> UpdateCategory(CategoryPutDto dto)
        {
            if(dto != null)
            {
                var category = await _repository.GetById(dto.Id);
                if (category != null)
                {
                    var entity = await _repository.Update(_mapper.Map<CategoryEntity>(dto));
                    entity.Status = category.Status;
                    var result = await _repository.SaveChangesAsync();
                   
                    if(result > 0) return true;
                }
            }
            return false;
        }
        public async Task<bool> ActivateCategory(Guid id)
        {
                var category = await _repository.GetById(id);
                if (category != null)
                {
                    category.Status = StoreStatusEnum.Active;
                    var entity = await _repository.Update(_mapper.Map<CategoryEntity>(category));
                    var result = await _repository.SaveChangesAsync();
                    if(result > 0) return true;
                }
            return false;
        }
        public async Task<bool> DeleteCategory(Guid id)
        {
                var category = await _repository.GetById(id);
                if (category != null)
                {
                    category.Status = StoreStatusEnum.Removed;
                    var entity = await _repository.Update(_mapper.Map<CategoryEntity>(category));
                    var result = await _repository.SaveChangesAsync();
                    if(result > 0) return true;
                }
            return false;
        }
    }
}
