﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Store.Domain.Entities.Configurations
{
    public class CategoryConfiguration :IEntityTypeConfiguration<CategoryEntity>
    {
        public void Configure(EntityTypeBuilder<CategoryEntity> builder)
        {
            builder.ToTable("Categories");
            builder.HasKey(c => c.Id);
            builder.Property(c => c.Name).HasMaxLength(30).IsRequired();
            builder.Property(c => c.Description).HasMaxLength(255);
            builder.Property(c => c.Status).IsRequired();
        }
    }
    public class ProductConfiguration : IEntityTypeConfiguration<ProductEntity>
    {
        public void Configure(EntityTypeBuilder<ProductEntity> builder)
        {
            builder.ToTable("Products");
            builder.HasKey(c => c.Id);
            builder.Property(c => c.CategoryId).IsRequired();
            builder.Property(c => c.AttributeId).IsRequired();
            builder.Property(c => c.GalleryId).IsRequired(false);
            builder.Property(c => c.Name).HasMaxLength(30).IsRequired();
            builder.Property(c => c.Description).HasMaxLength(255);
            builder.Property(c => c.SKU).HasMaxLength(20).IsRequired();
            builder.Property(c => c.Price).HasPrecision(18,2);
            builder.Property(c => c.Volume).HasPrecision(18,2);
            builder.Property(c => c.Status).IsRequired();
        }
    }
    public class AttributeConfiguration : IEntityTypeConfiguration<AttributeEntity>
    {
        public void Configure(EntityTypeBuilder<AttributeEntity> builder)
        {
            builder.ToTable("Attributes");
            builder.HasKey(k => k.Id);
            builder.Property(k => k.Id).IsRequired();
            builder.Property(k => k.Type).IsRequired();
            builder.Property(k => k.Unit).HasMaxLength(30).IsRequired();
            builder.Property(k => k.Status).IsRequired();

        }
    }
    public class GalleryConfiguration : IEntityTypeConfiguration<GalleryEntity>
    {
        public void Configure(EntityTypeBuilder<GalleryEntity> builder)
        {
            builder.ToTable("Galleries");
            builder.HasKey(k => k.Id);
            builder.Property(k => k.FileLink).IsRequired();
            builder.Property(k => k.Name).HasMaxLength(255).IsRequired();
            builder.Property(k => k.Description).HasMaxLength(255).IsRequired();
            builder.Property(k => k.Status).IsRequired();

        }
    }
}
