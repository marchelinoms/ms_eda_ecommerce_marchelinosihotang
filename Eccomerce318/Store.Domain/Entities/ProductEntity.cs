﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Store.Domain.Entities
{
    public class ProductEntity
    {
        public Guid Id { get; set; }
        public Guid CategoryId { get; set; } = default!;
        public Guid AttributeId { get; set; } = default!;
        public Guid? GalleryId { get; set; }
        public string SKU { get; set; } = default!;
        public string Name { get; set; } = default!;
        public string Description { get; set; } = default!;
        public decimal? Price { get; set; }
        public decimal? Volume { get; set; }
        public int Sold { get; set; }
        public int Stock { get; set; }
        public StoreStatusEnum Status { get; set; } = default!;
        public DateTime Modified { get; internal set; } = DateTime.Now;
        
        //Relationship
        [ForeignKey("CategoryId")]
        public virtual CategoryEntity Category { get; set; }
        [ForeignKey("AttributeId")]
        public virtual AttributeEntity Attribute { get; set;}
        [ForeignKey("GalleryId")]
        public virtual GalleryEntity Gallery { get; set;}
    }
}
