﻿namespace Store.Domain.Entities
{
    public class GalleryEntity
    {
        public Guid Id { get; set; }
        public string FileLink { get; set; } = default!;
        public string Name { get; set; } = default!;
        public string Description { get; set; } = default!;
        public StoreStatusEnum Status { get; set; } = default!;
        public DateTime Modified { get; internal set; } = DateTime.Now;
    }
}
