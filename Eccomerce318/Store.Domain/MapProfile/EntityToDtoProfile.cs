﻿using AutoMapper;
using Store.Domain.Dtos;
using Store.Domain.Entities;

namespace Store.Domain.MapProfile
{
    public class EntityToDtoProfile : Profile
    {
        public EntityToDtoProfile() : base("Entity to Dto Profile")
        {
            //Category Mapping
            CreateMap<CategoryEntity, CategoryDto> ();
            CreateMap<CategoryDto, CategoryEntity> ();
            CreateMap<CategoryEntity,CategoryPutDto>();
            CreateMap<CategoryPutDto,CategoryEntity>();

            //Product Mapping
            CreateMap<ProductEntity, ProductDto>();
            CreateMap<ProductDto, ProductEntity>();
            CreateMap<ProductEntity, ProductPutDto>();
            CreateMap<ProductPutDto, ProductEntity>();

            //Gallery Mapping
            CreateMap<GalleryEntity, GalleryDto>();
            CreateMap<GalleryDto, GalleryEntity>();
        }
    }
}
