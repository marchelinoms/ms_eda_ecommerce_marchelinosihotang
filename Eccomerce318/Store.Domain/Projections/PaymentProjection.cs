﻿
using Framework.Core.Events;
using Store.Domain.Entities;

namespace Store.Domain.Projections
{
    public record PaymentCreated(
       Guid CartId,
       List<CartProductItem> CartProducts,
       CartStatusEnum Status
   );

    public class CartProductItem
    {
        public Guid ProductId { get; set; }
        public int Stock { get; set; }
        public int Quantity { get; set; }
    }

    public class PaymentProjection
    {
        public static bool Handle(EventEnvelope<PaymentCreated> eventEnvelope)
        {
            var (cartId, cartProducts, status) = eventEnvelope.Data;
            using (var context = new StoreDbContext(StoreDbContext.OnConfigure()))
            {
                if (status == CartStatusEnum.Paid)
                {
                    foreach (var cp in cartProducts)
                    {
                        var product = context.Products.Where(o => o.Id == cp.ProductId).FirstOrDefault();
                        if (product != null)
                        {
                            product.Stock = cp.Stock;
                            product.Sold += cp.Quantity;
                            context.Products.Update(product);
                        }
                    }
                }

                context.SaveChanges();
            }

            return true;
        }
    }

}
