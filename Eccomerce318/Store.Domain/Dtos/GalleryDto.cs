﻿namespace Store.Domain.Dtos
{
    public class GalleryDto
    {
        public Guid Id { get; set; }
        public string FileLink { get; set; } = default!;
        public string Name { get; set; } = default!;
        public string Description { get; set; } = default!;
        public StoreStatusEnum Status { get; set; } = default!;
    }
}
