﻿
namespace Store.Domain.Dtos
{
    public class ProductDto
    {
        public Guid Id { get; set; }
        public Guid CategoryId { get; set; } = default!;
        public Guid AttributeId { get; set; } = default!;
        public string SKU { get; set; } = default!;
        public string Name { get; set; } = default!;
        public string Description { get; set; } = default!;
        public decimal? Price { get; set; }
        public decimal? Volume { get; set; }
        public int Sold { get; set; }
        public int Stock { get; set; }
        public StoreStatusEnum Status { get; set; } = default!;
    }
}
