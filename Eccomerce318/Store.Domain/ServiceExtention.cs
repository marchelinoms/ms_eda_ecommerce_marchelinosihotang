﻿using Store.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Store.Domain
{
    public static class ServiceExtention
    {
        public static void AddDomainContext(
            this IServiceCollection services, 
            Action<DbContextOptionsBuilder> optionAction, 
            ServiceLifetime contextLifeTime = ServiceLifetime.Scoped, 
            ServiceLifetime optionLifeTime = ServiceLifetime.Scoped) 
        {
            services.AddDbContext<StoreDbContext>(optionAction,contextLifeTime,optionLifeTime);
        }
    }
}
