﻿namespace Store.Domain.EventEnvelopes
{
    public record ProductStatusChanged(
        Guid Id,StoreStatusEnum Status,DateTime Modified)
    {
        public static ProductStatusChanged Chang(
            Guid Id, StoreStatusEnum Status,DateTime Modified) =>
            new(Id,Status,Modified);
    }
}
