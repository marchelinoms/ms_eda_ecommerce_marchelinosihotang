﻿namespace Store.Domain.EventEnvelopes
{
    public record ProductUpdated(
        Guid Id, Guid CategoryId, Guid AttributeId,
        string SKU, string Name, string Descrpition, 
        decimal Price, decimal Volume, int Sold,
        int Stock, DateTime Modified)
    {
        public static ProductUpdated Update(
            Guid Id, Guid CategoryId, Guid AttributeId, 
            string SKU, string Name, string Descrpition,
            decimal Price, decimal Volume, int Sold, int Stock,DateTime Modified) =>
            new(Id, CategoryId, AttributeId, 
                SKU, Name, Descrpition,
                Price, Volume, Sold,
                Stock,Modified);
    }
}
