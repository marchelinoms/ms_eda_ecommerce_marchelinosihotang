﻿namespace Store.Domain.EventEnvelopes
{
    public record ProductCreated(
        Guid Id, Guid CategoryId, Guid AttributeId,
        string SKU, string Name, string Descrpition,
        decimal Price, decimal Volume, int Sold,
        int Stock, StoreStatusEnum Status, DateTime Modified)
    {
        public static ProductCreated Create(
            Guid Id, Guid CategoryId, Guid AttributeId,
            string SKU, string Name, string Descrpition,
            decimal Price, decimal Volume, int Sold, int Stock, StoreStatusEnum Status, DateTime Modified) =>
            new(Id, CategoryId, AttributeId,
                SKU, Name, Descrpition,
                Price, Volume, Sold,
                Stock, Status, Modified);
    }
}
