﻿using Microsoft.EntityFrameworkCore;
using Store.Domain.Entities;

namespace Store.Domain.Repositories
{
    public interface IGalleryRepostitory
    {
        Task<IEnumerable<GalleryEntity>> GetAll();
        Task<GalleryEntity> GetById(Guid id);
        Task<GalleryEntity>Add(GalleryEntity entity);
        Task<int>SaveChangesAsync(CancellationToken cancellationToken = default);
    }
    public class GalleryRepository : IGalleryRepostitory
    {
        protected readonly StoreDbContext _context;
        public GalleryRepository(StoreDbContext context)
        {
            _context = context;
            _context.Database.EnsureCreated();
        }
        public async Task<GalleryEntity> Add(GalleryEntity entity)
        {
            await _context.Set<GalleryEntity>().AddAsync(entity);
            return entity;
        }

        public async Task<IEnumerable<GalleryEntity>> GetAll()
        {
            return await _context.Set<GalleryEntity>().Where(x => !x.Status.Equals(StoreStatusEnum.Removed)).ToListAsync();
        }

        public async Task<GalleryEntity> GetById(Guid id)
        {
            return await _context.Set<GalleryEntity>().FindAsync(id);
        }

        public async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            return await _context.SaveChangesAsync(cancellationToken);
        }
    }
}
