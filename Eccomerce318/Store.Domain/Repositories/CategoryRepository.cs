﻿using Store.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Store.Domain.Repositories
{
    public interface ICategoryRepository
    {
        Task<IEnumerable<CategoryEntity>> GetAll();
        Task<int> GetCount();
        Task<IEnumerable<CategoryEntity>> GetPaged(int Page, int Size);
        Task<CategoryEntity> GetById(Guid id);
        Task<CategoryEntity> Add(CategoryEntity entity);
        Task<CategoryEntity> Update(CategoryEntity entity);
        void Delete(CategoryEntity entity);
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
    }
    public class CategoryRepository : ICategoryRepository
    {
        protected readonly StoreDbContext _context;

        public CategoryRepository(StoreDbContext context)
        {
            _context = context;
            _context.Database.EnsureCreated();
        }

        public async Task<CategoryEntity> Add(CategoryEntity entity)
        {
           _context.Set<CategoryEntity>().Add(entity);
            return entity;
        }

        public void Delete(CategoryEntity entity)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<CategoryEntity>> GetAll()
        {
            return await _context.Set<CategoryEntity>().Where(x => !x.Status.Equals(StoreStatusEnum.Removed)).ToListAsync();
        }

        public async Task<CategoryEntity> GetById(Guid id)
        {
            return await _context.Set<CategoryEntity>().FindAsync(id);
        }

        public Task<int> GetCount()
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<CategoryEntity>> GetPaged(int Page, int Size)
        {
            throw new NotImplementedException();
        }

        public async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            return await _context.SaveChangesAsync(cancellationToken);
        }

        public async Task<CategoryEntity> Update(CategoryEntity entity)
        {
            _context.Set<CategoryEntity>().Update(entity);
            return entity;
        }
        public virtual void Dispose(bool disposing)
        {
            if (disposing)
                _context.Dispose();
        }
        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
