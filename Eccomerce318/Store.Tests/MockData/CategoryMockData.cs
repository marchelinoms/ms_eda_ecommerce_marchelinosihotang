﻿using Store.Domain.Entities;
using Store.Domain;
using System;
using System.Collections.Generic;

namespace Store.Tests.MockData
{
    public class CategoryMockData
    {
        public static List<CategoryEntity> GetCategories()
        {
            return new List<CategoryEntity>
            {
                new CategoryEntity
                {
                    Id = new Guid("4A1FF57A-9FF1-47A9-92C1-D464A28CF45C"),
                    Name = "Food",
                    Description = "Makanan Utama",
                    Status = StoreStatusEnum.Active
                },
                new CategoryEntity
                {
                    Id = new Guid("C87B2C34-FB87-42D7-AB35-67B0A5DC4DF7"),
                    Name = "Drink",
                    Description = "Minuman Segar",
                    Status = StoreStatusEnum.Inactive
                }
            };
        }
    }
}
