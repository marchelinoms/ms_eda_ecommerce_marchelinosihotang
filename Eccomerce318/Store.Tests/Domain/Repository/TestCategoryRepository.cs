﻿
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Store.Domain.Entities;
using Store.Domain.Repositories;
using Store.Tests.MockData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace Store.Tests.Domain.Repository
{
    public class TestCategoryRepository : IDisposable
    {
        protected readonly StoreDbContext _dbContext;
        private readonly ITestOutputHelper _outputHelper;

        public TestCategoryRepository(ITestOutputHelper outputHelper)
        {
            var options = new DbContextOptionsBuilder<StoreDbContext>().UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString()).Options;
            _dbContext = new StoreDbContext(options);
            _outputHelper = outputHelper;
        }

        [Fact]
        public async Task GetAllAsync_ReturnCategoriesCollection()
        {
            // Arrange
            _dbContext.Categories.AddRange(CategoryMockData.GetCategories());  
            _dbContext.SaveChanges();
            var repo = new CategoryRepository(_dbContext);
            // Action
            var result = await repo.GetById(new Guid("4A1FF57A-9FF1-47A9-92C1-D464A28CF45C"));
            _outputHelper.WriteLine($"Id : {result.Id} \n Name: {result.Name}");
            // Assert
            result.Name.Should().Be("Food") ;
        }
        public void Dispose()
        {
            _dbContext.Database.EnsureDeleted();
            _dbContext.Dispose();
        }
    }
}
