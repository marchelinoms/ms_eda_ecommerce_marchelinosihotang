using Carts.Domain;
using Carts.Domain.MapProfile;
using Carts.Domain.Repositories;
using Carts.Domain.Services;
using Carts.GraphQL.Schema.Mutation;
using Carts.GraphQL.Schema.Queries;
using Framework.Core.Events;
using Framework.Kafka;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace Carts.GraphQL
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.
            builder.Services.AddDomainContext(options =>
            {
                var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile(
                    "appsettings.json", optional: true, reloadOnChange: true);
                options.UseSqlServer(builder.Build().GetSection("ConnectionStrings").GetSection("Cart_Db_Conn").Value);
                options.EnableSensitiveDataLogging(false).UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            });
            builder.Services.AddControllers();
            
            builder.Services.AddCart();

            builder.Services.AddAutoMapper(config =>
            {
                config.AddProfile<EntityToDtoProfile>();
            });

            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();

            builder.Services.AddEventBus();
            builder.Services.AddKafkaProducerAndConsumer();
            builder.Services
            .AddScoped<CartQuery>()
            .AddScoped<CartProductQuery>()
            .AddScoped<CartMutation>()
            .AddScoped<CartProductMutation>()
            .AddScoped<ICartService, CartService>()
            .AddScoped<ICartRepository, CartRepository>()
            .AddScoped<ICartProductService, CartProductService>()
            .AddScoped<ICartProductRepository, CartProductRepository>()
            .AddScoped<IProductRepository, ProductRepository>()
            .AddScoped<IProductService, ProductService>()
            .AddGraphQLServer()
            .AddType<CartQuery>()
            .AddType<CartProductQuery>()
            .AddType<CartMutation>()
            .AddType<CartProductMutation>()
            .AddQueryType(q => q.Name("CartQuery"))
            .AddMutationType(m => m.Name("CartMutation"))
            .AddAuthorization();

            builder.Services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer("Bearer", options =>
            {
                var Configuration = builder.Configuration;
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = Configuration["JWT:ValidIssuer"],
                    ValidAudience = Configuration["JWT:ValidAudience"],
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["JWT:Secret"]))
                };
                options.Events = new JwtBearerEvents
                {
                    OnChallenge = context =>
                    {
                        context.Response.OnStarting(async () =>
                        {
                            await context.Response.WriteAsync("Account not authorized");
                        });
                        return Task.CompletedTask;
                    },
                    OnForbidden = context =>
                    {
                        context.Response.OnStarting(async () =>
                        {
                            await context.Response.WriteAsync("Forbidden Account");
                        });
                        return Task.CompletedTask;
                    }
                };
            });

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseHttpsRedirection();

            app.UseAuthorization();

            app.MapGraphQL();

            app.MapControllers();

            app.Run();
        }
    }
}