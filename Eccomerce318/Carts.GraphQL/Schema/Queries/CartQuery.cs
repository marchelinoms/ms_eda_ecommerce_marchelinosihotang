﻿using Carts.Domain.Dtos.Cart;
using Carts.Domain.Services;

namespace Carts.GraphQL.Schema.Queries
{
    [ExtendObjectType(Name = "CartQuery")]
    public class CartQuery
    {
        private readonly ICartService _service;
        public CartQuery(ICartService service)
        {
            _service = service;
        }
        public async Task<IEnumerable<CartDto>> GetCartAll()
        {
            IEnumerable<CartDto> result = await _service.All();
            return result;
        }
        public async Task<CartDto> GetCartById(Guid id)
        {
            return await _service.GetCartById(id);
        }
    }
}
