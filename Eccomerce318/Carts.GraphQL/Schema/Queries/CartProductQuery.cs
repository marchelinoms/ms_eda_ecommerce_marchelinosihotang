﻿using Carts.Domain.Dtos.CartProduct;
using Carts.Domain.Services;
using HotChocolate.Authorization;
using System.Data;

namespace Carts.GraphQL.Schema.Queries
{
    public class CartProductQuery
    {
        private readonly ICartProductService _service;

        public CartProductQuery(ICartProductService service)
        {
            _service = service;
        }

        [Authorize(Roles = new[] { "admin", "customer" })]
        public async Task<IEnumerable<CartProductDto>> getAllCartProduct()
        {
            IEnumerable<CartProductDto> result = await _service.GetAllCartProduct();
            return result;
        }

        [Authorize(Roles = new[] { "admin", "customer" })]
        public async Task<CartProductDto> getCartProductById(Guid id)
        {
            CartProductDto result = await _service.GetCartProductById(id);
            return result;
        }

        [Authorize(Roles = new[] { "admin", "customer" })]
        public async Task<IEnumerable<CartProductDto>> getCartProductByIdCart(Guid id)
        {
            IEnumerable<CartProductDto> result = await _service.GetCart(id);
            return result;
        }


    }
}
