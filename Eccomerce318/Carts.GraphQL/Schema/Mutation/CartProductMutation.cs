﻿using Carts.Domain.Dtos.CartProduct;
using Carts.Domain.Services;
using HotChocolate.Authorization;
using System.Data;

namespace Carts.GraphQL.Schema.Mutation
{
    [ExtendObjectType(Name = "CartMutation")]
    public class CartProductMutation
    {
        private readonly ICartProductService _service;

        public CartProductMutation(ICartProductService service)
        {
            _service = service;
        }

        [Authorize(Roles = new[] { "admin", "customer" })]
        public async Task<CartProductDto> addCartProduct(CartProductInputDto input)
        {
            CartProductDto dto = new CartProductDto();
            dto.CartId = input.CartId;
            dto.ProductId = input.ProductId;
            dto.Quantity = input.Quantity;

            var result = await _service.AddCartProduct(dto);

            return result;
        }

    }
}
