﻿using Carts.Domain;
using Carts.Domain.Dtos.Cart;
using Carts.Domain.Services;
using HotChocolate.Authorization;



namespace Carts.GraphQL.Schema.Mutation
{
    [ExtendObjectType(Name = "CartMutation")]
    public class CartMutation
    {
        private readonly ICartService _service;

        public CartMutation(ICartService service)
        {
            _service = service;
        }

        [Authorize(Roles = new[] { "admin", "customer" })]
        public async Task<CartDto> addCart(CartInputDto input)
        {
            CartDto dto = new CartDto();
            dto.CustomerId = input.CustomerId;
            var result = await _service.AddCart(dto);

            return result;
        }

        [Authorize(Roles = new[] { "admin", "customer" })]
        public async Task<CartDto> confirmCartStatus(Guid id)
        {
            var result = await _service.UpdateCartStatus(CartStatusEnum.Confirmed, id);
            if (!result)
            {
                throw new GraphQLException(new Error("Cart not found", "404"));
            }

            return await _service.GetCartById(id);
        }
    }

}

