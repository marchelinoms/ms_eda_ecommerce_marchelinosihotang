﻿using AutoMapper;
using Framework.Core.Events;
using Framework.Core.Events.Externals;
using User.Domain.Dtos;
using User.Domain.Entities;
using User.Domain.EventEnvelopes;
using User.Domain.Repositories;

namespace User.Domain.Services
{
    public interface IUserService
    {
        Task<IEnumerable<UserDto>> All();
        Task<UserDto> GetUserById(Guid id);
        Task<UserDto> AddUser(UserDto dto);
        Task<bool> UpdateUser(UserPutDto dto);
        Task<bool> ActivateUser(Guid id);
        Task<bool> DeleteUser(Guid id);
        Task<LoginDto> Login(string username, string password);
    }
    public class UserService : IUserService
    {
        private IUserRepository _repository;
        private readonly IMapper _mapper;
        private readonly IExternalEventProducer _externalEventProducer;

        public UserService(IUserRepository repository, IMapper mapper, IExternalEventProducer externalEventProducer)
        {
            _repository = repository;
            _mapper = mapper;
            _externalEventProducer = externalEventProducer;
        }
        public async Task<IEnumerable<UserDto>> All()
        {
            return _mapper.Map<IEnumerable<UserDto>>(await _repository.GetAll());
        }
        public async Task<UserDto> GetUserById(Guid id)
        {
            if(id != Guid.Empty)
            {
                var result = await _repository.GetById(id);
                if (result != null)
                    return _mapper.Map<UserDto>(result);
            }

            return null;
        }
        public async Task<LoginDto> Login (string username, string password)
        {
            var entity  = await _repository.Login(username, password);
            if(entity != null)
            {
                LoginDto dto = _mapper.Map<LoginDto>(entity);
                dto.Roles.Add(entity.Type.ToString());
                return dto;
            }
            return null;
        }
        public async Task<UserDto> AddUser(UserDto dto)
        {
            if (dto != null) 
            {
                dto.StatusEnum = UserStatusEnum.Inactive;
                var entity = await _repository.Add(_mapper.Map<UserEntity>(dto));
                var result = await _repository.SaveChangesAsync();
                var externalEvent = new EventEnvelope<UserCreated>(
                    UserCreated.Create(
                        entity.Id,
                        entity.UserName,
                        entity.Password,
                        entity.FirstName,
                        entity.LastName,
                        entity.Email,
                        entity.Type,
                        entity.StatusEnum,
                        entity.Modified));
                if (result > 0)
                    await _externalEventProducer.Publish(externalEvent, new CancellationToken());
                return _mapper.Map<UserDto>(entity);
            }
            return new UserDto();
        }
        public async Task<bool> UpdateUser(UserPutDto dto)
        {
            if(dto != null)
            {
                var user = await _repository.GetById(dto.Id);
                if(user != null)
                {
                    var entity = await _repository.Update(_mapper.Map<UserEntity>(dto));
                    entity.StatusEnum = user.StatusEnum;
                    var result = await _repository.SaveChangesAsync();

                    if(result > 0)
                    {
                        var externalEvent = new EventEnvelope<UserUpdated>(
                    UserUpdated.Update(
                        entity.Id,
                        entity.UserName,
                        entity.FirstName,
                        entity.LastName,
                        entity.Email,
                        entity.Type,
                        entity.Modified));
                        if (result > 0)
                            await _externalEventProducer.Publish(externalEvent, new CancellationToken());
                        return true;
                    }
                }
            }
            return false;
        }
        public async Task<bool> ActivateUser(Guid id)
        {
            var user = await _repository.GetById(id);
            if(user != null)
            {
                if (user.StatusEnum.Equals(UserStatusEnum.Inactive))
                {
                    user.StatusEnum = UserStatusEnum.Active;
                }
                else
                    user.StatusEnum = UserStatusEnum.Inactive;

                var entity = await _repository.Update(_mapper.Map<UserEntity>(user));
                var result = await _repository.SaveChangesAsync();
                if(result > 0)
                {
                    var externalEvent = new EventEnvelope<UserStatusChanged>(
                    UserStatusChanged.Change(
                        entity.Id,
                        entity.StatusEnum,
                        entity.Modified
                        ));
                    await _externalEventProducer.Publish(externalEvent,new CancellationToken());
                    return true;
                }
            }
            return false;
        }
        public async Task<bool> DeleteUser(Guid id)
        {
            var user = await _repository.GetById(id);
            if (user != null)
            {
                user.StatusEnum = UserStatusEnum.Removed;
                var entity = await _repository.Update(_mapper.Map<UserEntity>(user));
                var result = await _repository.SaveChangesAsync();
                if (result > 0)
                {
                    var externalEvent = new EventEnvelope<UserStatusChanged>(
                    UserStatusChanged.Change(
                        entity.Id,
                        entity.StatusEnum,
                        entity.Modified
                        ));
                    await _externalEventProducer.Publish(externalEvent, new CancellationToken());
                    return true;
                }
            }
            return false;
        }
    }

}
