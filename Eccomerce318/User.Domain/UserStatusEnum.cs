﻿namespace User.Domain
{
    public enum UserStatusEnum
    {
        Inactive,
        Active,
        Removed
    }
}
