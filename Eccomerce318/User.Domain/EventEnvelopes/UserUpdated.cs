﻿
namespace User.Domain.EventEnvelopes
{
    public record UserUpdated (Guid id,string UserName, string FirstName, string LastName, string Email, UserTypeEnum Type, DateTime Modified)
    {
        public static UserUpdated Update(
            Guid id, string UserName, string FirstName, string LastName, string Email, UserTypeEnum Type, DateTime Modified) => new (id, UserName, FirstName, LastName, Email, Type, Modified);   
    }
}
