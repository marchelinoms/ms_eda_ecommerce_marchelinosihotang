﻿
namespace User.Domain.EventEnvelopes
{
    public record UserStatusChanged (Guid id,UserStatusEnum StatusEnum, DateTime Modified)
    {
        public static UserStatusChanged Change(Guid id, UserStatusEnum StatusEnum, DateTime Modified) => new(id, StatusEnum, Modified);  
    }
}
