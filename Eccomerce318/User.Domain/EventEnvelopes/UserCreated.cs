﻿
namespace User.Domain.EventEnvelopes
{
    public record UserCreated (Guid id,string UserName, string Password, string FirstName, string LastName, string Email, UserTypeEnum Type, UserStatusEnum Status, DateTime Modified)
    {
        public static UserCreated Create(
            Guid id, string UserName, string Password, string FirstName, string LastName, string Email, UserTypeEnum Type, UserStatusEnum Status, DateTime Modified) => new (id, UserName, Password, FirstName, LastName, Email, Type, Status, Modified);   
    }
}
    