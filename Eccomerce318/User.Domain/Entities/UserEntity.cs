﻿namespace User.Domain.Entities
{
    public class UserEntity
    {
        public Guid Id { get; set; }
        public string UserName { get; set; } = default!;
        public string Password { get; set; } = default!;
        public string FirstName { get; set; } = default!;
        public string LastName { get; set; } = default!;
        public string Email { get; set; } = default!;   
        public UserTypeEnum Type { get; set; }
        public UserStatusEnum StatusEnum { get; set; }
        public DateTime Modified { get; internal set; } = DateTime.Now;
    }
}
