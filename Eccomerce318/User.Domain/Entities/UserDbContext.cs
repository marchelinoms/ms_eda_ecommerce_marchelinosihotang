﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace User.Domain.Entities
{
    public class UserDbContext :DbContext
    {
        public UserDbContext(DbContextOptions<UserDbContext>options) : base(options)
        {
            
        }
        public DbSet<UserEntity> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new UserConfiguration());
        }
        public static DbContextOptions<UserDbContext> OnConfigure()
        {
            var optionsBuilder = new DbContextOptionsBuilder<UserDbContext>();
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            optionsBuilder.UseSqlServer(builder.Build().GetSection("ConnectionStrings").GetSection("User_Db_Conn").Value);

            return optionsBuilder.Options;
        }
    }
}
