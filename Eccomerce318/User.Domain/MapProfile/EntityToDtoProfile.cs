﻿using AutoMapper;
using User.Domain.Dtos;
using User.Domain.Entities;

namespace User.Domain.MapProfile
{
    public class EntityToDtoProfile : Profile
    {
        public EntityToDtoProfile() : base("Entity to Dto Profile")
        {
            CreateMap<UserEntity, UserDto>();
            CreateMap<UserDto, UserEntity>();

            CreateMap<UserEntity,UserPutDto> ();
            CreateMap<UserPutDto, UserEntity>();

            CreateMap<UserEntity, LoginDto>();
            CreateMap<LoginDto, UserEntity>();
        }
    }
}
