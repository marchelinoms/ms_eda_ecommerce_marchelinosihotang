﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using User.Domain.Entities;

namespace User.Domain
{
    public static class ServiceExtention
    {
        public static void AddDomainContext(
            this IServiceCollection services,
            Action<DbContextOptionsBuilder> optionAction,
            ServiceLifetime contextLifeTime = ServiceLifetime.Scoped,
            ServiceLifetime optionLifeTime = ServiceLifetime.Scoped)
        {
            services.AddDbContext<UserDbContext>(optionAction, contextLifeTime, optionLifeTime);
        }
    }
}
