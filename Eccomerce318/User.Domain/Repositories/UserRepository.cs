﻿using Framework.Auth;
using Microsoft.EntityFrameworkCore;
using User.Domain.Entities;

namespace User.Domain.Repositories
{
    public interface IUserRepository
    {
        Task<IEnumerable<UserEntity>> GetAll();
        Task<UserEntity> GetById(Guid id);
        Task<UserEntity> Add(UserEntity entity);
        Task<UserEntity> Update(UserEntity entity);
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
        Task<UserEntity>Login(string username,string password);
    }
    public class UserRepository : IUserRepository
    {
        protected readonly UserDbContext _context;

        public UserRepository(UserDbContext context)
        {
            _context = context;
            _context.Database.EnsureCreated();
        }
        public async Task<UserEntity> Add(UserEntity entity)
        {
            _context.Set<UserEntity>().Add(entity);
            return entity;
        }
        public async Task<IEnumerable<UserEntity>> GetAll()
        {
            return await _context.Set<UserEntity>().Where(x => !x.StatusEnum.Equals(UserStatusEnum.Removed)).ToListAsync();
        }
        public async Task<UserEntity> GetById(Guid id)
        {
            return  await _context.Set<UserEntity>().FindAsync(id);
        }
        public async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            return await _context.SaveChangesAsync(cancellationToken);
        }
        public async Task<UserEntity>Update(UserEntity entity)
        {
            _context.Set<UserEntity>().Update(entity);
            return entity;
        }
        public async Task<UserEntity> Login(string username,string password)
        {
            return await _context.Set<UserEntity>().FirstOrDefaultAsync(x => x.UserName == username && x.Password == Encryption.HashSha256(password));
        }
        public virtual void Dispose(bool disposing)
        {
            if (disposing)
                _context.Dispose();
        }
        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
