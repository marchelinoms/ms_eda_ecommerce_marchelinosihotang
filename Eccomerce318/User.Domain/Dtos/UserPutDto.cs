﻿namespace User.Domain.Dtos
{
    public class UserPutDto
    {
        public Guid Id { get; set; }
        public string UserName { get; set; } = default!;
        public string FirstName { get; set; } = default!;
        public string LastName { get; set; } = default!;
        public string Email { get; set; } = default!;
        public UserTypeEnum Type { get; set; }
    }
        
}
