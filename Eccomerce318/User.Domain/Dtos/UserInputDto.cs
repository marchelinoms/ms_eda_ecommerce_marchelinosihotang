﻿namespace User.Domain.Dtos
{
    public class UserInputDto
    {
        public string UserName { get; set; } = default!;
        public string Password { get; set; } = default!;
        public string FirstName { get; set; } = default!;
        public string LastName { get; set; } = default!;
        public string Email { get; set; } = default!;
        public UserTypeEnum Type { get; set; }
    }
}
