﻿namespace Gateway.GraphQL.Extensions
{
    public static class IServiceCollectionExtensions
    {
        public const string Lookups = "LookupService";
        public const string Stores = "StoreService";
        public const string Carts = "CartService";
        public const string Users = "UserService";
        public static IServiceCollection AddHttpClientServices(this IServiceCollection services)
        {
            services.AddHttpClient(Lookups,client => client.BaseAddress = new Uri("https://localhost:7158/graphql"));
            services.AddHttpClient(Stores,client => client.BaseAddress = new Uri("https://localhost:7056/graphql"));
            services.AddHttpClient(Carts, client => client.BaseAddress = new Uri("https://localhost:7054/graphql"));
            services.AddHttpClient(Users,client => client.BaseAddress = new Uri("https://localhost:7078/graphql"));

            services
                .AddGraphQLServer()
                .AddRemoteSchema(Lookups)
                .AddRemoteSchema(Stores)
                .AddRemoteSchema(Carts)
                .AddRemoteSchema(Users);

            return services;
        }
    }
}
