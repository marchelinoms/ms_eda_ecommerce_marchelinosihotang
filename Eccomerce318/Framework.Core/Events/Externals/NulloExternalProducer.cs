﻿namespace Framework.Core.Events.Externals
{
    public class NulloExternalProducer : IExternalEventProducer
    {
        public Task Publish(IEventEnvelope @event, CancellationToken cancellation)
        {
            return Task.CompletedTask;
        }
    }
}
