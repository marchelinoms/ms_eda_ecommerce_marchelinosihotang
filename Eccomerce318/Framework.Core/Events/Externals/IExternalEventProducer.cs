﻿namespace Framework.Core.Events.Externals
{
    public interface IExternalEventProducer
    {
        Task Publish(IEventEnvelope @event,CancellationToken cancellation);
    }
}
