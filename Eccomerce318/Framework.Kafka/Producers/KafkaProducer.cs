﻿using Confluent.Kafka;
using Framework.Core.Events;
using Framework.Core.Events.Externals;
using Framework.Core.Serialization.Newtonsoft;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Framework.Kafka.Producers
{
    public class KafkaProducer : IExternalEventProducer
    {
        private readonly KafkaProducerConfig _config;
        private readonly ILogger<KafkaProducer> _logger;

        public KafkaProducer(IConfiguration config, ILogger<KafkaProducer> logger)
        {
            _config = config.GetKafkaProducerConfig();
            _logger = logger;
        }
        public async Task Publish(IEventEnvelope @event, CancellationToken cancellation)
        {
            try
            {
                using var p = new ProducerBuilder<string, string>(_config.ProducerConfig).Build();

                await p.ProduceAsync(_config.Topic, new Message<string, string>
                {
                    Key = @event.Data.GetType().Name,
                    Value = @event.ToJson() 
                }, cancellation).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error producing Kafka message : {ex.Message} {ex.StackTrace}");
                throw;
            }
        }
    }
}
