using Framework.Kafka;
using Store.Domain;
using Store.Domain.MapProfile;
using Microsoft.EntityFrameworkCore;
using Framework.Core.Events;
using Store.GraphQL.Schema.Queries;
using Store.Domain.Services;
using Store.Domain.Repositories;
using Store.GraphQL.Schema.Mutations;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.Extensions.FileProviders;

namespace Store.GraphQL
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.
            builder.Services.AddDomainContext(options =>
            {
                var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile(
                    "appsettings.json", optional: true, reloadOnChange: true);
                options.UseSqlServer(builder.Build().GetSection("ConnectionStrings").GetSection("Store_Db_Conn").Value);
                options.EnableSensitiveDataLogging(false).UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            });

            builder.Services.AddControllers();

            builder.Services.AddStore();

            builder.Services.AddAutoMapper(config =>
            {
                config.AddProfile<EntityToDtoProfile>();
            });
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();
            builder.Services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
           .AddJwtBearer("Bearer", options =>
           {
               var Configuration = builder.Configuration;
               options.TokenValidationParameters = new TokenValidationParameters
               {
                   ValidateIssuer = true,
                   ValidateAudience = true,
                   ValidateLifetime = true,
                   ValidateIssuerSigningKey = true,
                   ValidIssuer = Configuration["JWT:ValidIssuer"],
                   ValidAudience = Configuration["JWT:ValidAudience"],
                   IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["JWT:Secret"]))
               };
               options.Events = new JwtBearerEvents
               {
                   OnChallenge = context =>
                   {
                       context.Response.OnStarting(async () =>
                       {
                           await context.Response.WriteAsync("Account not authorized");
                       });
                       return Task.CompletedTask;
                   },
                   OnForbidden = context =>
                   {
                       context.Response.OnStarting(async () =>
                       {
                           await context.Response.WriteAsync("Forbidden Account");
                       });
                       return Task.CompletedTask;
                   }
               };
           });
            builder.Services.AddEventBus();
            builder.Services.AddKafkaProducerAndConsumer();
            builder.Services
             .AddScoped<ProductQuery>()
             .AddScoped<CategoryQuery>()
             .AddScoped<IProductRepository, ProductRepository>()
             .AddScoped<IProductService, ProductService>()
             .AddScoped<ICategoryRepository, CategoryRepository>()
             .AddScoped<ICategoryService, CategoryService>()
             .AddScoped<IGalleryRepostitory, GalleryRepository>()
             .AddScoped<IGalleryService, GalleryService>()
             .AddGraphQLServer()
             .AddAuthorization()
             .AddType<ProductQuery>()
             .AddType<CategoryQuery>()
             .AddType<ProductMutation>()
             .AddType<CategoryMutation>()
             .AddType<GalleryMutation>()
             .AddType<UploadType>()
             .AddQueryType(q => q.Name("StoreQuery"))
             .AddMutationType(m => m.Name("StoreMutation"));

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseHttpsRedirection();

            app.UseAuthorization();

            app.UseAuthentication();

            app.MapControllers();

            app.MapGraphQL();

            app.UseFileServer(new FileServerOptions()
            {
                FileProvider = new PhysicalFileProvider(
                    System.IO.Path.Combine(Directory.GetCurrentDirectory(), @"Resources/Images")),
                RequestPath = new PathString("/img"),
                EnableDirectoryBrowsing = true
            });

            app.Run();
        }
    }
}
