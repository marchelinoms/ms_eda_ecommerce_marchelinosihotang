﻿using Store.Domain.Dtos;
using Store.Domain.Services;

namespace Store.GraphQL.Schema.Queries
{
    [ExtendObjectType(Name = "StoreQuery")]
    public class ProductQuery
    {
        private readonly IProductService _service;

        public ProductQuery(IProductService service)
        {
            _service = service;
        }
        public async Task<IEnumerable<ProductDto>> GetAllProduct()
        {
            IEnumerable<ProductDto> result = await _service.All();
            return result;
        }
        public async Task<ProductDto> GetProductById(Guid id)
        {
            return await _service.GetProductById(id);
        }
    }
}
