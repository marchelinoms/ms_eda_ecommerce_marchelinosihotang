﻿using Store.Domain.Dtos;
using Store.Domain.Services;

namespace Store.GraphQL.Schema.Queries
{
    [ExtendObjectType(Name = "StoreQuery")]
    public class CategoryQuery
    {
        private readonly ICategoryService _service;

        public CategoryQuery(ICategoryService service)
        {
            _service = service;
        }
        public async Task<IEnumerable<CategoryDto>> GetAllCategory()
        {
            IEnumerable<CategoryDto> result = await _service.All();
            return result;
        }
        public async Task<CategoryDto> GetCategoryById(Guid id)
        {
            return await _service.GetCategoryById(id);
        }
    }
}
