﻿using Store.Domain.Dtos;
using Store.Domain.Services;
using Store.GraphQL.Schema.Mutations.Inputs;
using HotChocolate.Authorization;

namespace Store.GraphQL.Schema.Mutations
{
    [ExtendObjectType (Name = "StoreMutation")]
    public class ProductMutation
    {
        private readonly IProductService _service;

        public ProductMutation(IProductService service)
        {
            _service = service;
        }
        [Authorize(Roles = new[] { "admin" })]
        public async Task<ProductDto> AddProduct(ProductTypeInput product)
        {
            ProductDto dto = new ProductDto();
            dto.AttributeId = product.AttributeId;
            dto.CategoryId = product.CategoryId;
            dto.Name = product.Name;
            dto.Description = product.Description;
            dto.Price = product.Price;
            dto.SKU = product.SKU;
            dto.Sold = product.Sold;
            dto.Stock = product.Stock;
            dto.Volume = product.Volume;
            return await _service.AddProduct(dto);
        }
        [Authorize(Roles = new[] { "admin" })]
        public async Task<ProductPutDto> UpdateProduct(ProductTypeInput product, Guid id)
        {
            ProductPutDto dto = new ProductPutDto();
            dto.Id = id;
            dto.AttributeId = product.AttributeId;
            dto.CategoryId = product.CategoryId;
            dto.Name = product.Name;
            dto.Description = product.Description;
            dto.Price = product.Price;
            dto.SKU = product.SKU;
            dto.Sold = product.Sold;
            dto.Stock = product.Stock;
            dto.Volume = product.Volume;
            var result = await _service.UpdateProduct(dto);
            return result ? dto : throw new GraphQLException(new Error("Product not Found", "404"));
        }
        [Authorize(Roles = new[] { "admin" })]
        public async Task<ProductDto> UpdateProductStatus(Guid id)
        {
            ProductDto dto = await _service.GetProductById(id);
            var result = await _service.ActivateProduct(id);
            return result ? dto : throw new GraphQLException(new Error("Product not Found", "404"));
        }
        [Authorize(Roles = new[] { "admin" })]
        public async Task<string> DeleteProduct(Guid id)
        {
            ProductDto dto = await _service.GetProductById(id);
            var result = await _service.DeleteProduct(id);

            return result ? dto.Id + " has been deleted": throw new GraphQLException(new Error("Product not Found", "404"));
        }
    }
}
