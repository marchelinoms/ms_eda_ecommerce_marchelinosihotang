﻿using HotChocolate.Authorization;
using Store.Domain.Dtos;
using Store.Domain.Services;
using Store.GraphQL.Schema.Mutations.Inputs;
using System.Data;

namespace Store.GraphQL.Schema.Mutations
{
    [ExtendObjectType (Name = "StoreMutation")]
    public class CategoryMutation
    {
        private readonly ICategoryService _service;
        public CategoryMutation(ICategoryService service)
        {
            _service = service;
        }
        [Authorize(Roles = new[] { "admin" })]
        public async Task<CategoryDto> AddCategory(CategoryTypeInput category)
        {
            CategoryDto dto = new CategoryDto();
            dto.Name = category.Name;
            dto.Description = category.Description;
            return await _service.AddCategory(dto);
        }
        [Authorize(Roles = new[] { "admin" })]
        public async Task<CategoryPutDto> UpdateCategory(CategoryTypeInput category, Guid id)
        {
            CategoryPutDto dto = new CategoryPutDto();
            dto.Id = id;
            dto.Name = category.Name;
            dto.Description = category.Description;
            var result = await _service.UpdateCategory(dto);

            return result ? dto : throw new GraphQLException(new Error("Category not Found", "404"));
        }
        [Authorize(Roles = new[] { "admin" })]
        public async Task<CategoryDto> UpdateCategoryStatus(Guid id)
        {
            CategoryDto dto = await _service.GetCategoryById(id);
            var result = await _service.ActivateCategory(id);
            return result ? dto : throw new GraphQLException(new Error("Category not Found", "404"));
        }
        [Authorize(Roles = new[] { "admin" })]
        public async Task<string> DeleteCategory(Guid id)
        {
            CategoryDto dto = await _service.GetCategoryById(id);
            var result = await _service.DeleteCategory(id);
            return result ? dto.Id + " has been deleted" : throw new GraphQLException(new Error("Category not Found", "404"));
        }
    }
}
