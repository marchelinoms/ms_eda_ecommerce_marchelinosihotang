﻿namespace Store.GraphQL.Schema.Mutations.Inputs
{
    public class CategoryTypeInput
    {
        public string Name { get; set; } = default!;
        public string Description { get; set; } = default!;
    }
}
