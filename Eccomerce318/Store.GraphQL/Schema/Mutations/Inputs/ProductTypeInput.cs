﻿namespace Store.GraphQL.Schema.Mutations.Inputs
{
    public class ProductTypeInput
    {
        public string Name { get; set; } = default!;
        public string Description { get; set; } = default!;
        public Guid CategoryId { get; set; } = default!;
        public Guid AttributeId { get; set; } = default!;
        public string SKU { get; set; } = default!;
        public decimal? Price { get; set; }
        public decimal? Volume { get; set; }
        public int Sold { get; set; }
        public int Stock { get; set; }
    }
}
