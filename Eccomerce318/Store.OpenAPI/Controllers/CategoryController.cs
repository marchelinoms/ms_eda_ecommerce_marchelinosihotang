﻿using FluentValidation;
using FluentValidation.Results;
using Microsoft.AspNetCore.Mvc;
using Store.Domain.Dtos;
using Store.Domain.Services;


namespace Store.OpenAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly ICategoryService _service;
        private readonly ILogger<CategoryController> _logger;
        private readonly IValidator<CategoryDto> _validatorPost;
        private readonly IValidator<CategoryPutDto> _validatorPut;
        public CategoryController(ICategoryService service, ILogger<CategoryController> logger, IValidator<CategoryDto> validator, IValidator<CategoryPutDto> validator1)
        {
            _service = service;
            _logger = logger;
            _validatorPost = validator;
            _validatorPut = validator1;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(await _service.All());
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CategoryDto payload, CancellationToken cancellation)
        {
          
            try
            {
                ValidationResult result = await _validatorPost.ValidateAsync(payload);
                if (!result.IsValid) return BadRequest(result);
                
                var dto = await _service.AddCategory(payload);
                if(dto != null) return Ok(dto);
            }
            catch (OperationCanceledException ex) when (cancellation.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
            }
            return BadRequest();
        }

        [HttpPut]
        public async Task<IActionResult> Edit([FromBody] CategoryPutDto payload, CancellationToken cancellation)
        {
            try
            {
                if (payload != null)
                {

                    ValidationResult result = await _validatorPut.ValidateAsync(payload);
                    if (!result.IsValid) return BadRequest(result);
                    var isUpdated = await _service.UpdateCategory(payload);
                    return isUpdated ? Ok(isUpdated) : BadRequest();
                }
            }
            catch (OperationCanceledException ex) when (cancellation.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
            }
            return NoContent();
        }

        [HttpPut("Status")]
        public async Task<IActionResult> Activate(Guid id, CancellationToken cancellation)
        {
            try
            {
                var isUpdated = await _service.ActivateCategory(id);
                return isUpdated ? Ok(isUpdated) : BadRequest();

            }
            catch (OperationCanceledException ex) when (cancellation.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
            }
            return NoContent();
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(Guid id, CancellationToken cancellation)
        {
            try
            {
                var isUpdated = await _service.DeleteCategory(id);
                return isUpdated ? Ok(isUpdated) : BadRequest();

            }
            catch (OperationCanceledException ex) when (cancellation.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
            }
            return NoContent();
        }
    }
}
