﻿using FluentValidation;
using FluentValidation.Results;
using Microsoft.AspNetCore.Mvc;
using Store.Domain.Dtos;
using Store.Domain.Services;

namespace Store.OpenAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductService _service;
        private readonly ILogger<ProductController> _logger;
        private readonly IValidator<ProductDto> _validatorPost;
        private readonly IValidator<ProductPutDto> _validatorPut;

        public ProductController(IProductService service, ILogger<ProductController> logger, IValidator<ProductPutDto> validatorPut, IValidator<ProductDto> validatorPost)
        {
            _service = service;
            _logger = logger;
            _validatorPost = validatorPost;
            _validatorPut = validatorPut;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(await _service.All());
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] ProductDto payload, CancellationToken cancellation)
        {
            try
            {
                ValidationResult result = await _validatorPost.ValidateAsync(payload);
                if (!result.IsValid) return BadRequest(payload); 
                var dto = await _service.AddProduct(payload);
                if (dto != null) return Ok(dto);
            }
            catch (OperationCanceledException ex) when (cancellation.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
            }
            return BadRequest();
        }

        [HttpPut]
        public async Task<IActionResult> Edit([FromBody] ProductPutDto payload, CancellationToken cancellation)
        {
            try
            {
                if (payload != null)
                {

                    ValidationResult result = await _validatorPut.ValidateAsync(payload);
                    if (!result.IsValid) return BadRequest(payload);
                    var isUpdated = await _service.UpdateProduct(payload);
                    return isUpdated ? Ok(isUpdated) : BadRequest();
                }
            }
            catch (OperationCanceledException ex) when (cancellation.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
            }
            return NoContent();
        }

        [HttpPut("Status")]
        public async Task<IActionResult> Activate(Guid id, CancellationToken cancellation)
        {
            try
            {
                var isUpdated = await _service.ActivateProduct(id);
                return isUpdated ? Ok(isUpdated) : BadRequest();

            }
            catch (OperationCanceledException ex) when (cancellation.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
            }
            return NoContent();
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(Guid id, CancellationToken cancellation)
        {
            try
            {
                var isUpdated = await _service.DeleteProduct(id);
                return isUpdated ? Ok(isUpdated) : BadRequest();

            }
            catch (OperationCanceledException ex) when (cancellation.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
            }
            return NoContent();
        }
    }
}
