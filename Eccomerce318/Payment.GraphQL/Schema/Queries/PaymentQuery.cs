﻿using Payment.Domain.Dtos;
using Payment.Domain.Services;

namespace Payment.GraphQL.Schema.Queries
{
    [ExtendObjectType(Name = "PaymentQuery")]
    public class PaymentQuery
    {
        private readonly ICartService _service;
        public PaymentQuery(ICartService service)
        {
            _service = service;
        }

        public async Task<IEnumerable<CartDto>> GetAllAsync()
        {
            return await _service.All();
        }
    }
}
