﻿using Payment.Domain.Dtos;
using Payment.Domain.Services;

namespace Payment.GraphQL.Schema.Mutations
{
    [ExtendObjectType(Name = "PaymentMutation")]
    public class PaymentMutation
    {
        private readonly ICartService _service;
        public PaymentMutation(ICartService service)
        {
            _service = service;
        }
        public async Task<CartDto> Payment(Guid cartId, decimal amount)
        {
            return await _service.Pay(cartId, amount);
        }
    }
}
