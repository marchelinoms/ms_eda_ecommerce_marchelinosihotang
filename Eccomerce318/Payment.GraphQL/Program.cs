using Framework.Core.Events;
using Framework.Kafka;
using Payment.Domain;
using Payment.Domain.MapProfile;
using Payment.Domain.Repositories;
using Payment.Domain.Services;
using Payment.GraphQL.Schema.Mutations;
using Payment.GraphQL.Schema.Queries;
namespace Payment.GraphQL
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.
            builder.Services.AddDomainContext(builder.Configuration);
            builder.Services.AddControllers();
            builder.Services.AddAutoMapper(config =>
            {
                config.AddProfile<EntityToDtoProfile>();
            });
            builder.Services.AddPayment();
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();
            builder.Services.AddEventBus();
            builder.Services.AddKafkaProducerAndConsumer();
            builder.Services
            .AddScoped<PaymentQuery>()
            .AddScoped<PaymentMutation>()
            .AddScoped<ICartProductRepository, CartProductRepository>()
            .AddScoped<ICartRepository, CartRepository>()
            .AddScoped<IProductRepository, ProductRepository>()
            .AddScoped<ICartService, CartService>()
            .AddGraphQLServer()
            .AddType<PaymentQuery>()
            .AddType<PaymentMutation>()
            .AddQueryType(q => q.Name("PaymentQuery"))
            .AddMutationType(m => m.Name("PaymentMutation"));
            //.AddAuthorization();

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseHttpsRedirection();

            app.UseAuthorization();

            app.MapControllers();

            app.MapGraphQL();


            app.Run();
        }
    }
}