﻿using Carts.Domain.Entities;
using Framework.Core.Events;

namespace Carts.Domain.Projections
{
    public record UserCreated(Guid id, string UserName, string Password, string FirstName, string LastName, string Email, UserTypeEnum Type, UserStatusEnum Status, DateTime Modified);
    public record UserStatusChanged(Guid id, UserStatusEnum StatusEnum, DateTime Modified);
    public record UserUpdated(Guid id, string UserName, string FirstName, string LastName, string Email, UserTypeEnum Type, DateTime Modified);
    public record UserTypeChanged(
    Guid Id,
    UserTypeEnum Type,
    DateTime Modified
    );

    public class UserProjection
    {
        public UserProjection()
        {
            
        }
        public static bool Handle(EventEnvelope<UserCreated> eventEnvelope)
        {
            var (id, username, password, firstName, lastName, email, type, status, modified) = eventEnvelope.Data;
            using (var context = new CartDbContext(CartDbContext.OnConfigure()))
            {
                UserEntity entity = new UserEntity()
                {
                    Id = (Guid)id,
                    UserName = username,
                    Password = password,
                    FirstName = firstName,
                    LastName = lastName,
                    Email = email,
                    Type = type,
                    StatusEnum = status,
                    Modified = modified
                };
                context.Users.Add(entity);
                context.SaveChanges();
            }
            return true;
        }
        public static bool Handle(EventEnvelope<UserUpdated>eventEnvelope)
        {
            var (id, username, firstName, lastName, email, type, modified) = eventEnvelope.Data;
            using (var context = new CartDbContext(CartDbContext.OnConfigure()))
            {
                UserEntity entity = context.Set<UserEntity>().Find(id);
                entity.UserName = username;
                entity.FirstName = firstName;
                entity.LastName = lastName;
                entity.Email = email;
                entity.Type = type;
                entity.Modified = modified;
                context.SaveChanges();
            }
            return true;
        }
        public static bool Handle(EventEnvelope<UserStatusChanged>eventEnvelope)
        {
            var (id, status, modified) = eventEnvelope.Data;
            using (var context = new CartDbContext(CartDbContext.OnConfigure()))
            {
                UserEntity entity = context.Set<UserEntity>().Find(id);
                entity.StatusEnum = status;
                entity.Modified = modified;
                context.SaveChanges();
            }
            return true;
        }
        public static bool Handle(EventEnvelope<UserTypeChanged> eventEnvelope)
        {
            var (id, type, modified) = eventEnvelope.Data;
            using (var context = new CartDbContext(CartDbContext.OnConfigure()))
            {
                UserEntity entity = context.Set<UserEntity>().Find(id);

                entity.Id = (Guid)id;
                entity.Type = type;
                entity.Modified = modified;

                context.SaveChanges();
            }
            return true;
        }

    }
}
