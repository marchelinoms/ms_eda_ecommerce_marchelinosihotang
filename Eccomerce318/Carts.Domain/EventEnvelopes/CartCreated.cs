﻿
namespace Carts.Domain.EventEnvelopes
{
    public record CartCreated(
        Guid Id,
        Guid CustomerId,
        CartStatusEnum Status,
        DateTime Modified
        )
    {
        public static CartCreated Created(
            Guid id,
            Guid customerId,
            CartStatusEnum status,
            DateTime modified
            ) => new(id, customerId, status, modified);
    }
}
