﻿using AutoMapper;
using Carts.Domain.Dtos.CartProduct;
using Carts.Domain.Entities;
using Carts.Domain.Repositories;

namespace Carts.Domain.Services
{
    public interface ICartProductService
    {
        Task<CartProductDto> AddCartProduct(CartProductDto dto);
        Task<bool> UpdateCartProduct(CartProductDto dto);
        Task<IEnumerable<CartProductDto>> GetAllCartProduct();
        Task<CartProductDto> GetCartProductById(Guid id);
        Task<IEnumerable<CartProductDto>> GetCart(Guid id);
    }
    public class CartProductService : ICartProductService
    {
        private readonly ICartProductRepository _repository;
        private readonly IMapper _mapper;
        public CartProductService(ICartProductRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }
        public async Task<CartProductDto> AddCartProduct(CartProductDto dto)
        {
           var entity = await _repository.Add(_mapper.Map<CartProductEntity>(dto));
           var result = await _repository.SaveChangesAsync();
           if (result > 0)
                return _mapper.Map<CartProductDto>(entity);
            return new CartProductDto();
        }
        public async Task<bool> UpdateCartProduct(CartProductDto dto)
        {
            var entity = await _repository.Update(_mapper.Map<CartProductEntity>(dto));
            var result = await _repository.SaveChangesAsync();
            if (result > 0) return true;
            return false;
        }
        public async Task<IEnumerable<CartProductDto>> GetAllCartProduct()
        {
            return _mapper.Map<IEnumerable<CartProductDto>>(await _repository.GetAll());
        }
        public async Task<CartProductDto>GetCartProductById(Guid id)
        {
            if(id != Guid.Empty)
            {
                var cartProduct = await _repository.GetById(id);
                return _mapper.Map<CartProductDto>(cartProduct);
            }
            return null;
        }
        public async Task<IEnumerable<CartProductDto>> GetCart(Guid id)
        {
            return _mapper.Map<IEnumerable<CartProductDto>>(await _repository.GetCartById(id));
        }
    }
}
