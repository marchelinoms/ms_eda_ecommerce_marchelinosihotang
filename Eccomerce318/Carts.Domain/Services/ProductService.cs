﻿using AutoMapper;
using Carts.Domain.Dtos.Product;
using Carts.Domain.Repositories;

namespace Carts.Domain.Services
{
    public interface IProductService
    {
        Task<ProductDto> GetById(Guid id);
    }
    public class ProductService : IProductService
    {
        private IProductRepository _repository;
        private readonly IMapper _mapper;
        public ProductService(IProductRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<ProductDto> GetById(Guid id)
        {
            return _mapper.Map<ProductDto>(await _repository.GetById(id));
        }
    }

}
