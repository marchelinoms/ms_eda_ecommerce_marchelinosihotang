﻿using AutoMapper;
using Carts.Domain.Dtos.Cart;
using Carts.Domain.Entities;
using Carts.Domain.EventEnvelopes;
using Carts.Domain.Repositories;
using Framework.Core.Events;
using Framework.Core.Events.Externals;
using System.Collections.Generic;

namespace Carts.Domain.Services
{
    public interface ICartService
    {
        Task<CartDto> AddCart(CartDto dto);
        Task<IEnumerable<CartDto>> All();
        Task<CartDto> GetCartById(Guid id);
        Task<bool> UpdateCartStatus(CartStatusEnum status, Guid id);
    }
    public class CartService : ICartService
    {
        private ICartRepository _repository;
        private ICartProductRepository _cartProductRepository;
        private readonly IMapper _mapper;
        private readonly IExternalEventProducer _externalEventProducer;
        public CartService(ICartRepository repository,ICartProductRepository cartProductRepository, IMapper mapper, IExternalEventProducer externalEventProducer)
        {
            _repository = repository;
            _mapper = mapper;
            _externalEventProducer = externalEventProducer;
            _cartProductRepository = cartProductRepository;
        }
        public async Task<CartDto> AddCart(CartDto dto)
        {
            if (dto != null)
            {
                dto.Status = CartStatusEnum.Pending;
                var entity = await _repository.Add(_mapper.Map<CartsEntity>(dto));
                var result = await _repository.SaveChangesAsync();

                if (result > 0)
                    return _mapper.Map<CartDto>(entity);
            }
            return new CartDto();
        }
        public async Task<IEnumerable<CartDto>> All()
        {
            return _mapper.Map<IEnumerable<CartDto>>(await _repository.GetAll());
        }
        public async Task<CartDto> GetCartById(Guid id)
        {
            if(id != Guid.Empty)
            {
                var result = await _repository.GetById(id);
                if(result != null)
                {
                    return _mapper.Map<CartDto>(result);
                }
            }
            return null;
        }
        public async Task<bool> UpdateCartStatus(CartStatusEnum status, Guid id)
        {
            var cart = await _repository.GetById(id);
            if (cart != null)
            {
                cart.Status = status;
                var entity = await _repository.Update(cart);
                var result = await _repository.SaveChangesAsync();
                if (result > 0)
                {
                    List<CartProductItem> items = new List<CartProductItem>();
                    if(status == CartStatusEnum.Confirmed)
                    {
                        var cartProducts = await _cartProductRepository.GetCartById(cart.Id);
                        foreach (var item in cartProducts) 
                        {
                            items.Add(new CartProductItem()
                            {
                                Id = item.Id,
                                ProductId = item.ProductId,
                                Quantity = item.Quantity
                            });
                        }
                    }
                var externalEvent = new EventEnvelope<CartStatusChanged>(
                         CartStatusChanged.UpdateStatus(
                             entity.Id,
                             entity.CustomerId,
                             entity.Status,
                             items,
                             entity.Modified
                             )
                         );

                await _externalEventProducer.Publish(externalEvent, new CancellationToken());

                return true;
                }
            }
            return false;
        }
    }
}
