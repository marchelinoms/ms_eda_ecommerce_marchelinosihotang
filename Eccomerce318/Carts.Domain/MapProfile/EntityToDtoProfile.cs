﻿using AutoMapper;
using Carts.Domain.Dtos.Cart;
using Carts.Domain.Dtos.CartProduct;
using Carts.Domain.Dtos.Product;
using Carts.Domain.Dtos.User;
using Carts.Domain.Entities;


namespace Carts.Domain.MapProfile
{
    public class EntityToDtoProfile : Profile
    {
        public EntityToDtoProfile(): base("Entity to Dto Profile") 
        {
            //Cart Mapping
            CreateMap<CartsEntity, CartDto>();
            CreateMap<CartDto, CartsEntity>();

            //Product Mapping
            CreateMap<ProductEntity,ProductDto>();
            CreateMap<ProductDto, ProductEntity>();
            CreateMap<ProductEntity, ProductPutDto>();
            CreateMap<ProductPutDto, ProductEntity>();

            //CartProduct Mapping
            CreateMap<CartProductEntity, CartProductDto>();
            CreateMap<CartProductDto, CartProductEntity>();

            //User Mapping
            CreateMap<UserEntity, UserDto>();
            CreateMap<UserDto,UserEntity>();
            CreateMap<UserEntity, UserPutDto>();
            CreateMap<UserPutDto, UserEntity>();
        }
    }
}
