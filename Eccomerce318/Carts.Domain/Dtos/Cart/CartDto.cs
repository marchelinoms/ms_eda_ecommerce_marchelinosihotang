﻿namespace Carts.Domain.Dtos.Cart
{
    public class CartDto
    {
        public Guid Id { get; set; }
        public Guid CustomerId { get; set; }
        public CartStatusEnum Status { get; set; }
    }
}
