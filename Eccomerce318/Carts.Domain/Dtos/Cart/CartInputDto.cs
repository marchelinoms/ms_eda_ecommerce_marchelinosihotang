﻿namespace Carts.Domain.Dtos.Cart
{
    public class CartInputDto
    {
        public Guid CustomerId { get; set; }
    }
}
