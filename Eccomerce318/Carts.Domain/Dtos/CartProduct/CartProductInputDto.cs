﻿namespace Carts.Domain.Dtos.CartProduct
{
    public class CartProductInputDto
    {
        public Guid CartId { get; set; }
        public Guid ProductId { get; set; }
        //public string SKU { get; set; }
        //public string Name { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
    }
}
