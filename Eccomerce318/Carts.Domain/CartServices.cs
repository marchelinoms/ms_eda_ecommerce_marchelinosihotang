﻿using Framework.Core.Projection;
using Carts.Domain.Projections;
using Microsoft.Extensions.DependencyInjection;

namespace Carts.Domain
{
    public static class CartServices
    {
        public static IServiceCollection AddCart(this IServiceCollection services) => services.AddProjections();

        private static IServiceCollection AddProjections(this IServiceCollection services) => services
            .Projection(builder => builder
            .AddOn<ProductCreated>(ProductProjection.Handle)
            .AddOn<ProductUpdated>(ProductProjection.Handle)
            .AddOn<ProductStatusChanged>(ProductProjection.Handle)
            .AddOn<UserCreated>(UserProjection.Handle)
            .AddOn<UserUpdated>(UserProjection.Handle)
            .AddOn<UserTypeChanged>(UserProjection.Handle)
            .AddOn<UserStatusChanged>(UserProjection.Handle)
            .AddOn<PaymentCreated>(PaymentProjection.Handle)
            );

    }
   
}
