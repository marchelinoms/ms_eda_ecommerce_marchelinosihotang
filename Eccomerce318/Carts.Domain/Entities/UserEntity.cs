﻿namespace Carts.Domain.Entities
{
    public class UserEntity
    {
        public Guid Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string FirstName { get;set; }
        public string LastName { get;set; }
        public string Email { get; set; }   
        public UserTypeEnum Type { get; set; }
        public UserStatusEnum StatusEnum { get; set; }
        public DateTime Modified { get; internal set; } = DateTime.Now;
    }
}
