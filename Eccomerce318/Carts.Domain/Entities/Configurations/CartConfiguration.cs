﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Carts.Domain.Entities.Configurations
{
    public class CartConfiguration : IEntityTypeConfiguration<CartsEntity>
    {
        public void Configure(EntityTypeBuilder<CartsEntity> builder)
        {
            builder.ToTable("Carts");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.CustomerId).IsRequired();
            builder.Property(x => x.Status).IsRequired();
        }
    }
    public class ProductConfiguration : IEntityTypeConfiguration<ProductEntity>
    {
        public void Configure(EntityTypeBuilder<ProductEntity> builder)
        {
            builder.ToTable("Products");
            builder.HasKey(c => c.Id);
            builder.Property(c => c.CategoryId).IsRequired();
            builder.Property(c => c.AttributeId).IsRequired();

            builder.Property(c => c.Name).HasMaxLength(30).IsRequired();
            builder.Property(c => c.Description).HasMaxLength(255);
            builder.Property(c => c.SKU).HasMaxLength(20).IsRequired();
            builder.Property(c => c.Price).HasPrecision(18, 2);
            builder.Property(c => c.Volume).HasPrecision(18, 2);
            builder.Property(c => c.Status).IsRequired();
        }
    }
    public class CartProductConfiguration : IEntityTypeConfiguration<CartProductEntity>
    {
        public void Configure(EntityTypeBuilder<CartProductEntity> builder)
        {
            builder.ToTable("CartProducts");
            builder.HasKey(c => c.Id);
            builder.Property(c => c.CartId).IsRequired();
            builder.Property(c => c.ProductId).IsRequired();
            //builder.Property(c => c.SKU).HasMaxLength(20).IsRequired();
            //builder.Property(c => c.Name).HasMaxLength(30).IsRequired();
            builder.Property(c => c.Price).HasPrecision(18, 2);
            builder.Property(c => c.Quantity).IsRequired();
        }
    }
    public class UserConfiguration : IEntityTypeConfiguration<UserEntity>
    {
        public void Configure(EntityTypeBuilder<UserEntity> builder)
        {
            builder.ToTable("Users");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.UserName).IsRequired();
            builder.Property(x => x.Password).IsRequired();
            builder.Property(x => x.Email).IsRequired();
            builder.Property(x => x.FirstName).IsRequired();
            builder.Property(x => x.LastName).IsRequired();
            builder.Property(x => x.Type).IsRequired();
            builder.Property(x => x.StatusEnum).IsRequired();
        }
    }
}
