﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Carts.Domain.Entities
{
    public class CartsEntity
    {
        public Guid Id { get; set; }
        public Guid CustomerId { get; set; }
        public CartStatusEnum Status { get; set; }
        public DateTime Modified { get; internal set; } = DateTime.Now;

        [ForeignKey("CustomerId")]
        public virtual UserEntity Customer { get; set; }
        public virtual ICollection<CartProductEntity> CartProducts { get; set; }
    }


}
