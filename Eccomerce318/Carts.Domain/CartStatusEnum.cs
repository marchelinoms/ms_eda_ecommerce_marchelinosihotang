﻿namespace Carts.Domain
{
    public enum CartStatusEnum
    {
        Pending,
        Paid,
        Confirmed,
        Cancelled,
        Removed
    }
}
