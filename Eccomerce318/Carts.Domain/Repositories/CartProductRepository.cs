﻿using Carts.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Carts.Domain.Repositories
{
    public interface ICartProductRepository
    {
        Task<CartProductEntity> Add(CartProductEntity entity);
        Task<IEnumerable<CartProductEntity>> GetAll();
        Task<CartProductEntity> GetById(Guid id);
        Task<CartProductEntity> Update(CartProductEntity entity);
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
        Task<IEnumerable<CartProductEntity>> GetCartById(Guid id);
    }
    public class CartProductRepository : ICartProductRepository
    {
        protected readonly CartDbContext _context;
        public CartProductRepository(CartDbContext context)
        {
            _context = context;
            _context.Database.EnsureCreated();
        }
        public async Task<CartProductEntity> Add(CartProductEntity entity)
        {
            _context.Set<CartProductEntity>().Add(entity);
            return entity;
        }
        public async Task<IEnumerable<CartProductEntity>> GetAll()
        {
            return await _context.Set<CartProductEntity>().ToListAsync();
        }
        public async Task<CartProductEntity> GetById(Guid id)
        {
            return await _context.Set<CartProductEntity>().FindAsync(id);
        }
        public async Task<IEnumerable<CartProductEntity>> GetCartById(Guid id)
        {
            return await _context.Set<CartProductEntity>().Where(x => x.CartId == id).ToListAsync();
        }
        public async Task<CartProductEntity> Update(CartProductEntity entity)
        {
            _context.Set<CartProductEntity>().Update(entity);
            return entity;
        }
        public async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            return await _context.SaveChangesAsync(cancellationToken);
        }
        public virtual void Dispose(bool disposing)
        {
            if (disposing)
                _context.Dispose();
        }
        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
