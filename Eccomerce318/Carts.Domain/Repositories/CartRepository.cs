﻿using Carts.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Carts.Domain.Repositories
{
    public interface ICartRepository
    {
        Task<CartsEntity>Add(CartsEntity entity);
        Task<CartsEntity> Update(CartsEntity entity);
        Task<CartsEntity> GetById(Guid id);
        Task<IEnumerable<CartsEntity>> GetAll();
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
    }
    public class CartRepository : ICartRepository
    {
        protected readonly CartDbContext _context;
        public CartRepository(CartDbContext context)
        {
            _context = context;
            _context.Database.EnsureCreated();
        }
        public async Task<CartsEntity> Add(CartsEntity entity)
        {
            await _context.Set<CartsEntity>().AddAsync(entity);
            return entity;
        }
        public async Task<IEnumerable<CartsEntity>> GetAll()
        {
            return await _context.Set<CartsEntity>().Where(x => !x.Status.Equals(CartStatusEnum.Removed)).ToListAsync();
        }
        public async Task<CartsEntity>GetById(Guid id)
        {
            return await _context.Set<CartsEntity>().FindAsync(id);
        }
        public async Task<CartsEntity>Update(CartsEntity entity)
        {
            _context.Set<CartsEntity>().Update(entity);
            return entity;
        }
        public async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            return await _context.SaveChangesAsync(cancellationToken);
        }
        public virtual void Dispose(bool disposing) 
        {
            if(disposing)
                _context.Dispose();
        }
        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
