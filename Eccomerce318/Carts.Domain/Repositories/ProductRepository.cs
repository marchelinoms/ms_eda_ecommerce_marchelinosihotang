﻿
using Carts.Domain.Entities;

namespace Carts.Domain.Repositories
{
    public interface IProductRepository
    {
        Task<ProductEntity> GetById(Guid id);
    }
    public class ProductRepository : IProductRepository
    {
        protected readonly CartDbContext _context;

        public ProductRepository(CartDbContext context)
        {
            _context = context;
            _context.Database.EnsureCreated();
        }

        public async Task<ProductEntity> GetById(Guid id)
        {
            return await _context.Set<ProductEntity>().FindAsync(id);
        }
    }
}
