﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Carts.Domain
{
    public enum UserStatusEnum
    {
        Active,
        Inactive,
        Removed
    }
}
