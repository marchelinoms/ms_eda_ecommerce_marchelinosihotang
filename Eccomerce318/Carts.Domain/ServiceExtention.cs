﻿using Carts.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Carts.Domain
{
    public static class ServiceExtention
    {
        public static void AddDomainContext(
            this IServiceCollection services, 
            Action<DbContextOptionsBuilder> optionAction, 
            ServiceLifetime contextLifeTime = ServiceLifetime.Scoped, 
            ServiceLifetime optionLifeTime = ServiceLifetime.Scoped) 
        {
            services.AddDbContext<CartDbContext>(optionAction,contextLifeTime,optionLifeTime);
        }
    }
}
