﻿using System.Text.Json.Serialization;

namespace Payment.Domain
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum StoreStatusEnum
    {
        Active,
        Inactive,
        Removed
    }

    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum CartStatusEnum
    {
        Pending,
        Confirmed,
        Paid,
        Cancelled
    }

    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum UserStatusEnum
    {
        Active,
        Inactive,
        Removed
    }
}
