﻿namespace Payment.Domain.Dtos
{
    public class CartDto
    {
        public Guid Id { get; set; }
        public Guid CustomerId { get; set; }
        public decimal Total { get; set; }
        public decimal Pay { get; set; }
        public CartStatusEnum Status { get; set; }
        public DateTime Modified { get; set; }
        public UserDto Customer { get; set; }
    }

    public class UserDto
    {
        public Guid Id { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
    }
}
