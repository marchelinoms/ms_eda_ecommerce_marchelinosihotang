﻿using Framework.Core.Events;
using Payment.Domain.Entities;

namespace Payment.Domain.Projections
{
    public record ProductCreated(
        Guid Id,
        decimal Price,
        int Stock,
        StoreStatusEnum Status
    );

    public class ProductProjection
    {
        public static bool Handle(EventEnvelope<ProductCreated> eventEnvelope)
        {
            var (id, price, stock, status) = eventEnvelope.Data;
            using (var context = new PaymentDbContext(PaymentDbContext.OnConfigure()))
            {
                ProductEntity entity = new ProductEntity()
                {
                    Id = (Guid)id,
                    Price = price,
                    Stock = stock,
                    Status = status
                };

                context.Products.Add(entity);
                context.SaveChanges();
            }

            return true;
        }
    }
}
