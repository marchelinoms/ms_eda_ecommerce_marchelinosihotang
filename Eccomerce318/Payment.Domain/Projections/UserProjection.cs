﻿using Framework.Core.Events;
using Payment.Domain.Entities;

namespace Payment.Domain.Projections
{
    public record UserCreated(
        Guid Id,
        string UserName,
        string FirstName,
        string LastName,
        string Email,
        UserStatusEnum Status
    );

    public class UserProjection
    {
        public static bool Handle(EventEnvelope<UserCreated> eventEnvelope)
        {
            var (id, userName, firstName, lastName, email, status) = eventEnvelope.Data;
            using (var context = new PaymentDbContext(PaymentDbContext.OnConfigure()))
            {
                UserEntity entity = new UserEntity()
                {
                    Id = (Guid)id,
                    UserName = userName,
                    FirstName = firstName,
                    LastName = lastName,
                    Email = email
                };

                context.Users.Add(entity);
                context.SaveChanges();
            }

            return true;
        }
    }
}
