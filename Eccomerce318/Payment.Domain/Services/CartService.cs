﻿using AutoMapper;
using Framework.Core.Events;
using Framework.Core.Events.Externals;
using Payment.Domain.Dtos;
using Payment.Domain.Entities;
using Payment.Domain.EventEnvelopes;
using Payment.Domain.Repositories;

namespace Payment.Domain.Services
{
    public interface ICartService
    {
        Task<IEnumerable<CartDto>> All();
        Task<CartDto> Pay(Guid CartId, decimal amount);
    }
    public class CartService : ICartService
    {
        private readonly ICartRepository _cartRepository;
        private readonly ICartProductRepository _cartProductRepository;
        private readonly IProductRepository _productRepository;
        private readonly IMapper _mapper;
        private readonly IExternalEventProducer _externalEventProducer;
        public CartService(
            ICartRepository cartRepository,
            ICartProductRepository cartProductRepository,
            IProductRepository productRepository,
            IMapper mapper,
            IExternalEventProducer externalEventProducer)
        {
            _cartRepository = cartRepository;
            _cartProductRepository = cartProductRepository;
            _productRepository = productRepository;
            _mapper = mapper;
            _externalEventProducer = externalEventProducer;
        }

        public async Task<IEnumerable<CartDto>> All()
        {
            return _mapper.Map<IEnumerable<CartDto>>(await _cartRepository.All());
        }

        public async Task<CartDto> Pay(Guid CartId, decimal amount)
        {
            CartEntity entity = await _cartRepository.GetById(CartId);
            if (entity != null) 
            {
                if(entity.Total <= amount)
                {
                    var list = await _cartProductRepository.GetByCartId(CartId);
                    List<CartProductItem> cartProducts = _mapper.Map<IEnumerable<CartProductItem>>(list).ToList();
                    //Check and Update Stock
                    foreach (var item in list)
                    {
                        var product = await _productRepository.GetById(item.ProductId);
                        if(product.Status == StoreStatusEnum.Active && product.Stock >= item.Quantity)
                        {
                            product.Stock -= item.Quantity;
                            await _productRepository.Update(product);
                        }
                        else
                        {
                            var selected = cartProducts.Where(o => o.ProductId == item.ProductId).FirstOrDefault();
                            cartProducts.Remove(selected);
                        }
                    }
                    entity.Pay = amount;
                    entity.Status = CartStatusEnum.Paid;
                    await _cartRepository.Update(entity);
                    var result = await _cartRepository.SaveChangesAsync();
                    if (result > 0)
                    {
                        var externalEvent = new EventEnvelope<PaymentCreated>(
                            PaymentCreated.Create(
                                entity.Id,
                                cartProducts,
                                entity.Status
                            ));
                        await _externalEventProducer.Publish(externalEvent, new CancellationToken());
                        return _mapper.Map<CartDto>(entity);
                    }
                }
            }
            return null;
        }
    }
}
