﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Payment.Domain.Entities.Configuration;

namespace Payment.Domain.Entities
{
    public class PaymentDbContext : DbContext
    {
        public PaymentDbContext(DbContextOptions<PaymentDbContext> options) : base(options)
        {
            
        }
        public DbSet<CartEntity> Carts { get; set; }
        public DbSet<UserEntity> Users { get; set; }
        public DbSet<ProductEntity> Products { get; set; }
        public DbSet<CartProductEntity> CartProducts { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
                .ApplyConfiguration(new ProductConfiguration())
                .ApplyConfiguration(new UserConfiguration())
                .ApplyConfiguration(new CartConfiguration())
                .ApplyConfiguration(new CartProductConfiguration());
        }
        public static DbContextOptions<PaymentDbContext> OnConfigure()
        {
            var optionsBuilder = new DbContextOptionsBuilder<PaymentDbContext>();
            optionsBuilder.UseSqlServer(ServiceExtension.Configuration.GetConnectionString(ServiceExtension.DefaultConnection));
            return optionsBuilder.Options;
        }
    }
}
